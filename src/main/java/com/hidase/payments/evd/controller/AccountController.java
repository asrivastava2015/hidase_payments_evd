package com.hidase.payments.evd.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hidase.payments.evd.constants.HidasiePaymentsConstants;
import com.hidase.payments.evd.dto.AccountDTO;
import com.hidase.payments.evd.objects.AuthObjects;
import com.hidase.payments.evd.services.AccountServices;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/hidasiepayments/v1")
public class AccountController extends AuthObjects {

	@Autowired
	AccountServices accountService;

	/* Get all the accounts */
	@ApiOperation(value = "Get all the accounts")
	@GetMapping(value = "/accounts", produces = "application/json")
	public ResponseEntity<?> getAccounts(@RequestHeader("authorization") String authString) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.RETRIEVEALL);
		return new ResponseEntity<>(accountService.getAccounts(), HttpStatus.OK);
	}

	/* Get a particular account */
	@ApiOperation(value = "Get account with accountId")
	@GetMapping(value = "/accounts/{accountid}", produces = "application/json")
	public ResponseEntity<?> getAccount(@PathVariable(value = "accountid") long accountid,
			@RequestHeader("authorization") String authString) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService, HidasiePaymentsConstants.RETRIEVE);

		return new ResponseEntity<>(accountService.getAccount(accountid), HttpStatus.OK);
	}

	/* Get users details mapped with a accountId */
	@ApiOperation(value = "Get users details mapped with a account")
	@GetMapping(value = "/accounts/users/{accountid}", produces = "application/json")
	public ResponseEntity<?> getAccountsUser(@PathVariable(value = "accountid") long accountid,
			@RequestHeader("authorization") String authString) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService, HidasiePaymentsConstants.RETRIEVE);

		return new ResponseEntity<>(accountService.getAccountsUser(accountid), HttpStatus.OK);
	}

	/* Add new Account and a new user */
	@ApiOperation(value = "Add a account")
	@PostMapping(value = "/accounts/add", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> addAccount(@RequestBody @Valid AccountDTO account,
			@RequestHeader("authorization") String token) throws Exception {

		AuthObjects.basicAuth().isUserAuthenticated(token, accountService,
				HidasiePaymentsConstants.CREATEACCOUNTANDUSER);

		return new ResponseEntity<>(accountService.addAccount(account), HttpStatus.CREATED);
	}

	/* Delete account with accountid */
	@ApiOperation(value = "Delete a account with accountid")
	@DeleteMapping(value = "/delete/account/{accountid}", produces = "application/json")
	public ResponseEntity<?> deleteAccount(@PathVariable(value = "accountid") long accountid,
			@RequestHeader("authorization") String authString) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.DELETEACCOUNT);
		return new ResponseEntity<>(accountService.deleteAccount(accountid), HttpStatus.OK);
	}

	/* Get all the users */
	// To do to get all the user
	@ApiOperation(value = "Get all the users")
	@GetMapping(value = "/users", produces = "application/json")
	public ResponseEntity<?> getUsers(@RequestHeader("authorization") String authString) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.RETRIEVEALL);
		return new ResponseEntity<>(accountService.getUsers(), HttpStatus.OK);
	}

	/* Add user to a already existing account */
	@ApiOperation(value = "Add user to a already existing account with accountId")
	@PostMapping(value = "/accounts/add/user/{accountid}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> addUserAccount(@RequestBody @Valid AccountDTO account,
			@PathVariable(value = "accountid") long accountid, @RequestHeader("authorization") String authString)
			throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.CREATEUSEREXISTINGACCOUNT);
		return new ResponseEntity<>(accountService.addUserAccount(account, accountid), HttpStatus.CREATED);
	}

	/* Update the user with user id */
	@ApiOperation(value = "Update user with userId")
	@PutMapping(value = "/users/update/{userid}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> updateUser(@RequestBody @Valid AccountDTO account,
			@PathVariable(value = "userid") long userid, @RequestHeader("authorization") String authString)
			throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService, HidasiePaymentsConstants.UPDATEUSER);
		return new ResponseEntity<>(accountService.updateUser(account, userid), HttpStatus.OK);
	}

	/* Delete the user with user id */
	@ApiOperation(value = "Delete user with userId")
	@DeleteMapping(value = "/users/delete/{userid}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> deleteUser(@PathVariable(value = "userid") long userid,
			@RequestHeader("authorization") String authString) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService, HidasiePaymentsConstants.DELETEUSER);
		return new ResponseEntity<>(accountService.deleteUser(userid), HttpStatus.OK);
	}

	/* Get a user with userId */
	@ApiOperation(value = "Get user with userId")
	@GetMapping(value = "/users/{userid}", produces = "application/json")
	public ResponseEntity<?> getUser(@PathVariable(value = "userid") long userid,
			@RequestHeader("authorization") String authString) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService, HidasiePaymentsConstants.RETRIEVE);

		return new ResponseEntity<>(accountService.getUser(userid), HttpStatus.OK);
	}
	
	/* Get all the transactions for the account */
	@ApiOperation(value = "Get all the transactions for the account")
	@GetMapping(value = "/accounts/{accountId}/transactions", produces = "application/json")
	public ResponseEntity<?> getTransactions(@RequestHeader("authorization") String authString,@PathVariable(value = "accountId") long accountId) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.RETRIEVEALL);
		return new ResponseEntity<>(accountService.getTransactions(accountId), HttpStatus.OK);
	}
	
	/* Get prefund transactions for the account */
	@ApiOperation(value = "Get prefund transactions for the account")
	@GetMapping(value = "/accounts/{accountId}/transactions/prefund", produces = "application/json")
	public ResponseEntity<?> getPrefundTransactions(@RequestHeader("authorization") String authString,@PathVariable(value = "accountId") long accountId) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.RETRIEVEALL);
		return new ResponseEntity<>(accountService.getPrefundTransactions(accountId), HttpStatus.OK);
	}
	
	/* Get topup transactions for the account */
	@ApiOperation(value = "Get topup transactions for the account")
	@GetMapping(value = "/accounts/{accountId}/transactions/topup", produces = "application/json")
	public ResponseEntity<?> getTopUpTransactions(@RequestHeader("authorization") String authString,@PathVariable(value = "accountId") long accountId) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.RETRIEVEALL);
		return new ResponseEntity<>(accountService.getTopUpTransactions(accountId), HttpStatus.OK);
	}

}
