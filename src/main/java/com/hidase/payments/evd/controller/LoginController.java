package com.hidase.payments.evd.controller;


import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hidase.payments.evd.constants.HidasiePaymentsConstants;
import com.hidase.payments.evd.dto.AccountDTO;
import com.hidase.payments.evd.dto.PasswordDTO;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.exception.InvalidCredentialsException;
import com.hidase.payments.evd.exception.InvalidSessionException;
import com.hidase.payments.evd.exception.UnexpectedErrorException;
import com.hidase.payments.evd.exception.UserNotRegisteredException;
import com.hidase.payments.evd.objects.AuthObjects;
import com.hidase.payments.evd.repository.UserRepository;
import com.hidase.payments.evd.services.AccountServices;
import com.hidase.payments.evd.services.LoginService;
import com.hidase.payments.evd.utilities.CommonUtil;

@RestController
@RequestMapping("/hidasiepayments/v1")
public class LoginController {
	
	@Autowired
	AccountServices accountService;

	@Autowired
	private UserRepository userRepo;

	private final LoginService loginService;

	@Autowired
	public LoginController(final LoginService loginService) {
		this.loginService = loginService;
	}

	@PostMapping(value = "/login", produces = "application/json")
	public ResponseEntity<?> createSessionToken(@RequestHeader(name = "Authorization") String authorization)
			throws Exception {
		return loginService.createOrUpdateSessionToken(authorization);

	}

	@PostMapping(value = "/forgot-password", produces = "application/json")
	public ResponseEntity<Object> processForgotPasswordForm(@RequestBody @Valid AccountDTO account,
			HttpServletRequest request) throws UnsupportedEncodingException, MessagingException {
		String email = account.getEmail();
		User user = userRepo.findUserByEmail(email);
		if (user == null) {
			return new ResponseEntity<Object>(loginService.errorMessage(),HttpStatus.OK);
		}
		return new ResponseEntity<Object>(loginService.forgotPassword(account, request, user), HttpStatus.OK);

	}

	@PostMapping("/reset-password")
	public ResponseEntity<Object> processResetPassword(@RequestHeader("authorization") String authString,@RequestBody @Valid PasswordDTO password) {
		System.out.println("2^^^^password is" + password.getPassword());
		if(authString.contains("Bearer")) {
			authString=authString.replace("Bearer", "").trim();
		}
		User user = userRepo.findUserByToken(authString);
		if (user == null) {
			throw new UserNotRegisteredException("User is not registered.Please register the user");
		}
		if (!user.getToken().equals(authString)) {
			throw new InvalidSessionException("User session is expired");
		}
		if (user.getTokenExpiry().before(CommonUtil.getTimeStamp())) {
			throw new UnexpectedErrorException("User password provided is null");
		}
		if(!user.isTempPwd() || user.getTempPassword()==null ||user.getTempPassword().isEmpty()) {
			throw new UnexpectedErrorException("Temporary passowrd is not generated for the user");
		}
		
		return new ResponseEntity<Object>(loginService.resetPassword(user,password), HttpStatus.OK);
	}
	
	@PostMapping("/update-password")
	public ResponseEntity<Object> processupdatePassword(@RequestBody @Valid PasswordDTO password,
			@RequestHeader("authorization") String authString) throws Exception {
		User user=AuthObjects.basicAuth().isUserAuthenticate(authString, accountService, HidasiePaymentsConstants.RETRIEVE);

		if (password.getNewPassword() == null || password.getCurrentPassword()==null) {
			throw new UnexpectedErrorException("Provided Password is null");
		}
		return new ResponseEntity<Object>(loginService.updatePassword(user,password), HttpStatus.OK);
	}
	
	
	@PostMapping("/logout")
	public ResponseEntity<Object> logOut(@RequestHeader("authorization") String authString) throws Exception {
		User user=AuthObjects.basicAuth().isUserAuthenticate(authString, accountService, HidasiePaymentsConstants.RETRIEVE);
		return new ResponseEntity<Object>(loginService.logOut(user), HttpStatus.OK);
	}

}
