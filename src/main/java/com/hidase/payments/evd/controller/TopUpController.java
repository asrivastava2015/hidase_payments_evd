package com.hidase.payments.evd.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hidase.payments.evd.constants.HidasiePaymentsConstants;
import com.hidase.payments.evd.dto.TopUpDTO;
import com.hidase.payments.evd.objects.AuthObjects;
import com.hidase.payments.evd.services.AccountServices;
import com.hidase.payments.evd.services.TopUpServices;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/hidasiepayments/v1")
public class TopUpController {
	
	@Autowired
	private TopUpServices services;
	
	@Autowired
	private AccountServices accountService;

	/*TopUp*/
	@ApiOperation(value = "TopUp")
	@PostMapping(value = "/topup", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> topup(@RequestBody @Valid TopUpDTO topUp,
			@RequestHeader("authorization") String token) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(token, accountService, HidasiePaymentsConstants.TOPUP);
		return new ResponseEntity<>(services.topUp(topUp,token), HttpStatus.OK);
		
	}
}
