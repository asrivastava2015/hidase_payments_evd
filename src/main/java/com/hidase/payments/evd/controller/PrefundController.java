package com.hidase.payments.evd.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hidase.payments.evd.constants.HidasiePaymentsConstants;
import com.hidase.payments.evd.dto.PrefundActionDTO;
import com.hidase.payments.evd.dto.PrefundDTO;
import com.hidase.payments.evd.entities.Prefund;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.exception.PrefundNotFoundException;
import com.hidase.payments.evd.objects.AuthObjects;
import com.hidase.payments.evd.services.AccountServices;
import com.hidase.payments.evd.services.PrefundServices;
import com.hidase.payments.evd.utilities.CommonUtil;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/hidasiepayments/v1")
public class PrefundController {

	@Autowired
	AccountServices accountService;
	@Autowired
	PrefundServices prefundService;
	@Autowired
	CommonUtil commonUtil;

	/* Get all the prefunds */
	@ApiOperation(value = "Get all the prefunds raised")
	@GetMapping(value = "/prefunds", produces = "application/json")
	public ResponseEntity<List<Prefund>> getPrefunds(@RequestHeader("authorization") String authString)
			throws Exception {

		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.RETRIEVEALL);
	
		return new ResponseEntity<List<Prefund>>(prefundService.getPrefunds(), HttpStatus.OK);

	}

	/* Get all the prefunds raised against PrimayAccount */
	@ApiOperation(value = "Get all the prefunds raised against primaryaccount with primary accountId")
	@GetMapping(value = "/prefunds/accountPrefund/{accountId}", produces = "application/json")
	public List<List<Prefund>> getPrimaryAccountPrefunds(@PathVariable(value = "accountId") long accountId,
			@RequestHeader("authorization") String authString) throws Exception {

		User user=AuthObjects.basicAuth().isUserAuthenticate(authString, accountService, HidasiePaymentsConstants.RETRIEVE);
		commonUtil.verifyTheUserAccountId(user, authString, accountId);
		return prefundService.getPrimaryAccountPrefunds(accountId);
	}

	/* Get a particular prefund */
	@ApiOperation(value = "Get a prefund with prefundId")
	@GetMapping(value = "/prefunds/{prefundId}", produces = "application/json")
	public ResponseEntity<?> getPrefund(@PathVariable(value = "prefundId") long prefundId,
			@RequestHeader("authorization") String authString) throws Exception {

		User user = AuthObjects.basicAuth().isUserAuthenticate(authString, accountService,
				HidasiePaymentsConstants.RETRIEVE);
		commonUtil.verifyThePrefundId(user, authString, prefundId);
		return new ResponseEntity<>(prefundService.getPrefund(prefundId), HttpStatus.OK);

	}

	/* Create a prefund */
	@ApiOperation(value = "Raise a prefund Request")
	@PostMapping(value = "/prefunds/create", produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> createprefund(@RequestBody @Valid PrefundDTO prefund,
			@RequestHeader("authorization") String authString) throws Exception {
		User user = AuthObjects.basicAuth().isUserAuthenticate(authString, accountService,
				HidasiePaymentsConstants.PREFUNDREQUEST);
			return new ResponseEntity<>(prefundService.createPrefund(prefund, user), HttpStatus.CREATED);
	}

	/* Get a prefund for a particular user */
	@ApiOperation(value = "Get a prefund for a user with userId")
	@GetMapping(value = "/prefunds/user/{userId}", produces = "application/json")
	public ResponseEntity<?> getUserPrefund(@PathVariable(value = "userId") long userId,
			@RequestHeader("authorization") String authString) throws Exception {
		User user=AuthObjects.basicAuth().isUserAuthenticate(authString, accountService, HidasiePaymentsConstants.RETRIEVE);
		commonUtil.verifyTheUserId(user, authString, userId);
		try {
			return new ResponseEntity<>(prefundService.getUserPrefunds(userId), HttpStatus.OK);
		} catch (PrefundNotFoundException e) {

			return new ResponseEntity<>("No prefund is present for a user with provided user id" + " " + userId,
					HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>("Unable to fetch prefunds for the user", HttpStatus.OK);
		}
	}

	/* Get a prefund for a particular account */
	@ApiOperation(value = "Get all the prefunds raised for account with account id")
	@GetMapping(value = "/prefunds/account/{accountId}", produces = "application/json")
	public ResponseEntity<?> getAccountPrefund(@PathVariable(value = "accountId") long accountId,
			@RequestHeader("authorization") String authString) throws Exception {
		User user = AuthObjects.basicAuth().isUserAuthenticate(authString, accountService,
				HidasiePaymentsConstants.RETRIEVE);
		// To verify the if account id provided is either of the user or primary user or
		// not then throw Exception
		commonUtil.verifyTheUserAccountId(user, authString, accountId);
		return new ResponseEntity<>(prefundService.getAccountPrefunds(accountId), HttpStatus.OK);
	}

	/* Perform prefund action approve or reject prefund */
	@ApiOperation(value = "Approve or decline a prefund")
	@PostMapping(value = "/prefunds/action/{prefundId}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> prefundAction(@RequestBody @Valid PrefundActionDTO prefund,
			@PathVariable(value = "prefundId") long prefundId, @RequestHeader("authorization") String authString)
			throws Exception {
		User user = AuthObjects.basicAuth().isUserAuthenticate(authString, accountService,
				HidasiePaymentsConstants.PREFUNDACTION);
		long primaryAccountId = commonUtil.verifyprefundAction(user, authString, prefundId);
		return new ResponseEntity<>(prefundService.prefundAction(prefund, prefundId, primaryAccountId),
				HttpStatus.OK);
	}

}
