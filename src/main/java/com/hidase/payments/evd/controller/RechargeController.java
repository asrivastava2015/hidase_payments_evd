package com.hidase.payments.evd.controller;

import com.hidase.payments.evd.constants.HidasiePaymentsConstants;
import com.hidase.payments.evd.dto.MobileTopupDTO;
import com.hidase.payments.evd.dto.RechargeResponseDTO;
import com.hidase.payments.evd.objects.AuthObjects;
import com.hidase.payments.evd.services.AccountServices;
import com.hidase.payments.evd.services.TopUpServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hidasiepayments/v1/recharge")
public class RechargeController {


    private final AccountServices accountService;

    private final TopUpServices topUpServices;


    @Autowired
    public RechargeController(AccountServices accountService, TopUpServices topUpServices) {
        this.accountService = accountService;
        this.topUpServices = topUpServices;
    }

    @PostMapping(value = "/prepaid", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<RechargeResponseDTO> prepaidTransaction(@RequestHeader("authorization") String token, @RequestBody MobileTopupDTO dto) throws Exception {
        AuthObjects.basicAuth().isUserAuthenticated(token, accountService, HidasiePaymentsConstants.TOPUP);
        final RechargeResponseDTO rechargeResponseDTO = topUpServices.doRecharge(dto, token);
        return new ResponseEntity<RechargeResponseDTO>(rechargeResponseDTO, HttpStatus.OK);
    }

}
