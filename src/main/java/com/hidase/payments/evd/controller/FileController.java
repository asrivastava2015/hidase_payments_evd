package com.hidase.payments.evd.controller;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hidase.payments.evd.constants.HidasiePaymentsConstants;
import com.hidase.payments.evd.objects.AuthObjects;
import com.hidase.payments.evd.services.AccountServices;
import com.hidase.payments.evd.services.FileServices;

@RestController
@RequestMapping("/hidasiepayments/v1")
public class FileController {
	
	@Autowired
	FileServices fileService;
	@Autowired
	AccountServices accountService;

	@PostMapping(value = "/upload", consumes = "multipart/form-data")
	public ResponseEntity<?> uploadFile(@RequestHeader("authorization") String authString,@RequestParam("file") MultipartFile[] files) throws Exception {
		AuthObjects.basicAuth().isUserAuthenticated(authString, accountService,
				HidasiePaymentsConstants.RETRIEVEALL);
		return new ResponseEntity<>(fileService.readFile(files),HttpStatus.OK);
	}

}
