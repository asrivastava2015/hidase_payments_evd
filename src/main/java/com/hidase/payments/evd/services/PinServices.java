package com.hidase.payments.evd.services;


import org.springframework.stereotype.Service;
import com.hidase.payments.evd.entities.FileAudit;


@Service
public interface PinServices {

	FileAudit findByBatchNumber(String batchnumber) throws Exception;
	
}
