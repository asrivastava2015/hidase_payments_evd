package com.hidase.payments.evd.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.stereotype.Component;
import com.hidase.payments.evd.dto.AccountDTO;
import com.hidase.payments.evd.dto.LoginResponseDTO;
import com.hidase.payments.evd.dto.PasswordDTO;
import com.hidase.payments.evd.entities.Account;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.entities.UserToken;
import com.hidase.payments.evd.exception.InvalidCredentialsException;
import com.hidase.payments.evd.exception.UnexpectedErrorException;
import com.hidase.payments.evd.exception.UserNotRegisteredException;
import com.hidase.payments.evd.repository.AccountRepository;
import com.hidase.payments.evd.repository.UserRepository;
import com.hidase.payments.evd.services.AccountServices;
import com.hidase.payments.evd.services.LoginService;
import com.hidase.payments.evd.services.UserTokenService;
import com.hidase.payments.evd.utilities.CommonUtil;
import com.mysql.cj.exceptions.PasswordExpiredException;
import net.bytebuddy.utility.RandomString;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Objects;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

@Component
public class LoginServiceImpl implements LoginService {

	private final AccountServices accountServices;

	private final UserTokenService userTokenService;

	@Autowired
	private AccountRepository accountRepo;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private CommonUtil commounUtil;
	
	private boolean isTemporaryPassword;
	
	private boolean isSetPassword;

	@Autowired
	public LoginServiceImpl(final AccountServices accountServices, final UserTokenService userTokenService) {
		this.accountServices = accountServices;
		this.userTokenService = userTokenService;
	}

	@Override
	public ResponseEntity<LoginResponseDTO> createOrUpdateSessionToken(String authorization) throws Exception {
		User user = getUser(authorization);
		Account acct = accountRepo.findByUserId(user.getId());
		final UserToken userToken = userTokenService.createOrUpdateUserToken(user.getId());
		System.out.println("2^^^^^user account is" + acct);
		if (Objects.isNull(userToken)) {
			throw new UnexpectedErrorException("Error while creating or updating token");
		}
		System.out.println("2^^^token expiry is"+userToken.getTokenExpiry());
		System.out.println("2^^^^^^user tokn is"+userToken.getToken());
		return new ResponseEntity<LoginResponseDTO>(LoginResponseDTO.builder().tokenExpiry(userToken.getTokenExpiry())
				.mfaRequired(userToken.isMfaRequired()).userId(user.getId()).accountId(acct.getId())
				.tempPassword(user.isTempPwd()).token(userToken.getToken()).build(), HttpStatus.OK);

	}

	private User getUser(String authorization) throws Exception {
		isTemporaryPassword=false;
		isSetPassword=false;
		String[] token = authorization.split(" ");
		String decode = token[1].trim();
		byte[] decoded = Base64.getDecoder().decode(decode);
		String auth = new String(decoded);
		String[] authArr = auth.split(":");
		String email = authArr[0].trim();
		String password = authArr[1].trim();

		User user = accountServices.findUserByEmail(email);
		System.out.println("2^^^^user is" + user);
		System.out.println("2^^^^user  password" + password);
		if (Objects.isNull(user)) {
			throw new UserNotRegisteredException("User is not registered.Please register the user");
		}
		boolean isTemPassword = user.isTempPwd();
		String tempPassword = user.getTempPassword();
		String storedPass = user.getPassword();
		if(isTemPassword && tempPassword != null && !tempPassword.isEmpty()) {
			isTemporaryPassword=CommonUtil.checkPass(password, tempPassword);
		}
		if(storedPass!=null) {
			isSetPassword=CommonUtil.checkPass(password, storedPass);
		}
		
		if(!isTemporaryPassword && !isSetPassword) {
			throw new InvalidCredentialsException("Username or password is incorrect");
		}
		
		if(isTemporaryPassword) {
			if (user.getTempPasswordExpiry().before(CommonUtil.getTimeStamp())) {
				throw new PasswordExpiredException(
						"Your temporary password is expired. Please generate a new password");
			}
		}
		
		if(isSetPassword) {
			user.setTempPassword(null);
			user.setTempPwd(false);
			user.setTempPasswordExpiry(null);
			userRepo.save(user);
		}
		
		
		return user;
	}

	@Override
	public ResponseEntity<Object> forgotPassword(AccountDTO account, HttpServletRequest request, User user) throws UnsupportedEncodingException, MessagingException {

		String tempPassword = RandomString.make(10);
		user.setTempPassword(CommonUtil.hashPassword(tempPassword));
		user.setTempPwd(true);
		user.setTempPasswordExpiry(CommonUtil.getResetPasswordExpiry());
		userRepo.save(user);
        commounUtil.sendEmail(account.getEmail(), tempPassword);
		
		return new ResponseEntity<Object>("Your link to reset password is successfully sent to your registered email id.", HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> updatePassword(User user, PasswordDTO password) {
		String storedpass = user.getPassword();
		System.out.println("2^^^update password stored pass" + storedpass);
		System.out.println("2^^^^^update password user pass" + password.getCurrentPassword());

		if (!CommonUtil.checkPass(password.getCurrentPassword(), storedpass)) {
			throw new InvalidCredentialsException("Provided current password is incorrect");
		}

		user.setPassword(CommonUtil.hashPassword(password.getNewPassword()));
		user.setLastUpdated(CommonUtil.getTimeStamp());
		userRepo.save(user);
		return new ResponseEntity<Object>("Password is updated successfully", HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> resetPassword(User user, PasswordDTO password) {
		user.setPassword(CommonUtil.hashPassword(password.getPassword()));
		user.setTempPassword(null);
		user.setTempPasswordExpiry(null);
		user.setTempPwd(false);
		user.setLastUpdated(CommonUtil.getTimeStamp());
		userRepo.save(user);
		return new ResponseEntity<Object>("Password is reset successfully", HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> errorMessage() {
		return new ResponseEntity<Object>("No account is registered with the provided email address.Please provide the correct email address",HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<Object> logOut(User user) {
		user.setToken(null);
		user.setTokenExpiry(null);
		userRepo.save(user);
		return new ResponseEntity<Object>("Logged out successfully",HttpStatus.OK);
		
	}
}
