package com.hidase.payments.evd.services;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;


public interface FileServices {

	List<Object> readFile(MultipartFile[] files) throws Exception;
}
