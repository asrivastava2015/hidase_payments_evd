package com.hidase.payments.evd.services.impl;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hidase.payments.evd.entities.FileAudit;
import com.hidase.payments.evd.entities.Pin;
import com.hidase.payments.evd.exception.FileReadIncorrectly;
import com.hidase.payments.evd.repository.FileProcessingAuditRepository;
import com.hidase.payments.evd.repository.PinRepository;
import com.hidase.payments.evd.services.FileServices;
import com.hidase.payments.evd.services.PinServices;
import com.hidase.payments.evd.utilities.CommonUtil;

@Service
public class FileServiceImpl implements FileServices {

	private String serial;
	private String pinValue;
	private double facevalue;
	private String batchnumber;
	private String expiry;
	private double quantity;
	private double recordCount;
	private boolean startReading;
	private boolean fileAlreadyread;
	@Autowired
	private PinRepository pinRepo;
	@Autowired
	private PinServices pinService;
	@Autowired
	private FileProcessingAuditRepository fileAuditRepo;

	@Override
	public List<Object> readFile(MultipartFile[] files) throws Exception {
		
		List<Object> fileResponse=new ArrayList<>();
		for (int j = 0; j < files.length; j++) {
			recordCount = 0;
			quantity=0;
			fileAlreadyread = true;
			List<Pin> pinList = new ArrayList<>();
			HashMap<String,String> resMap=new HashMap<>();
			FileAudit fileAudit = null;

			Scanner myReader = new Scanner(files[j].getInputStream());
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				if (data.contains("PO Number:")) {
					continue;
				}
				if (data.contains("[END]")) {
					startReading = false;
					continue;
				}
				if (data.contains("Batch:")) {
					batchnumber = data.substring(data.indexOf(":") + 1);
				}
				if (data.contains("Quantity:")) {
					quantity = Double.parseDouble(data.substring(data.indexOf(":") + 1));
				}
				if (fileAlreadyread) {
					fileAudit = pinService.findByBatchNumber(batchnumber);
					if (fileAudit == null) {
						fileAlreadyread = false;
					}
				}

				if (fileAudit != null) {
					boolean isRead = fileAudit.isRead();
					if (isRead) {
						resMap.put("FileName",files[j].getOriginalFilename());
						resMap.put("Batch", batchnumber);
						resMap.put("Message","This file with batch number"+" "+batchnumber+" is already read on"+" "+fileAudit.getReadOn());
						resMap.put("Status","AlreadyProcessed");
						fileResponse.add(resMap);
						break;
					}
				}
				Pin pin = new Pin();
				
				if (data.contains("FaceValue:")) {
					facevalue = Double.parseDouble(data.substring(data.indexOf(":") + 1));
				}
				if (data.contains("StopDate:")) {
					expiry = data.substring(data.indexOf(":") + 1);
				}
				if (data.contains("[BEGIN]")) {
					startReading = true;
					continue;
				}
				if (startReading) {
					String[] pinData = data.split(" ");
					serial = pinData[0];
					pinValue = pinData[1];
					recordCount++;

					pin.setBatchNumber(batchnumber);
					pin.setExpiry(expiry);
					pin.setFaceValue(facevalue);
					pin.setPin(pinValue);
					pin.setSerial(serial);
					pin.setStatus("UNUSED");
					pin.setCustomId(pinValue + serial);
					pinList.add(pin);
				}

			}
			myReader.close();
			if (recordCount != quantity) {
				resMap.put("FileName",files[j].getOriginalFilename());
				resMap.put("Batch", batchnumber);
				resMap.put("Message","This file with batch number"+" "+batchnumber+" "+"is not read due to mismatch in quantity");
				resMap.put("Status","Failed");
				fileResponse.add(resMap);
				continue;
			}
			if (!pinList.isEmpty() && pinList.size() != 0) {
				for (int i = 0; i < pinList.size(); i++) {
					Pin pin = pinList.get(i);
					pinRepo.save(pin);
				}
			}
			if (fileAudit == null && pinList.size() != 0) {
				FileAudit file = new FileAudit();
				file.setFileName(files[j].getOriginalFilename());
				file.setBatchNumber(batchnumber);
				file.setRead(true);
				file.setReadOn(CommonUtil.getTimeStamp());
				fileAuditRepo.save(file);
			}
			
			if(!resMap.containsKey("FileName")) {
			resMap.put("FileName",files[j].getOriginalFilename());
			resMap.put("Batch", batchnumber);
			resMap.put("Message","This file with batch number"+" "+batchnumber+ "is proccessed successfully");
			resMap.put("Status","Success");
			fileResponse.add(resMap);
			}
			
		}
		return fileResponse;

	}

}
