package com.hidase.payments.evd.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hidase.payments.evd.entities.FileAudit;
import com.hidase.payments.evd.repository.FileProcessingAuditRepository;
import com.hidase.payments.evd.services.PinServices;


@Component
public class PinServicesImpl implements PinServices{

	@Autowired
	private FileProcessingAuditRepository fileRepo;
	@Override
	public FileAudit findByBatchNumber(String batchnumber) throws Exception {
		FileAudit audit=fileRepo.findByBatchNumber(batchnumber);
		return audit;
	}

}
