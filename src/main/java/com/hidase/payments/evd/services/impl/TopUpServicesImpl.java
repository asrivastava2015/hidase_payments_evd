package com.hidase.payments.evd.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hidase.payments.evd.client.RechargeClient;
import com.hidase.payments.evd.client.ResponseStatus;
import com.hidase.payments.evd.client.TransactionResponse;
import com.hidase.payments.evd.dto.MobileTopupDTO;
import com.hidase.payments.evd.dto.PinDTO;
import com.hidase.payments.evd.dto.RechargeResponseDTO;
import com.hidase.payments.evd.dto.ResponseDTO;
import com.hidase.payments.evd.dto.TopUpDTO;
import com.hidase.payments.evd.entities.Account;
import com.hidase.payments.evd.entities.TransactionStatus;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.exception.InSufficientBalanceException;
import com.hidase.payments.evd.repository.AccountRepository;
import com.hidase.payments.evd.repository.UserRepository;
import com.hidase.payments.evd.services.TopUpServices;
import com.hidase.payments.evd.utilities.CommonUtil;

@Component
public class TopUpServicesImpl implements TopUpServices {

	@Autowired
	private UserRepository userRepo;
	@Autowired
	private AccountRepository accountRepo;
	@Autowired
	private CommonUtil commonUtil;

	private final RechargeClient rechargeClient;

	@Autowired
	public TopUpServicesImpl(UserRepository userRepo, AccountRepository accountRepo, CommonUtil commonUtil,
			RechargeClient rechargeClient) {
		this.userRepo = userRepo;
		this.accountRepo = accountRepo;
		this.commonUtil = commonUtil;
		this.rechargeClient = rechargeClient;
	}

	@Override
	public PinDTO topUp(TopUpDTO topUp, String token) throws Exception {
		if (token.contains("Bearer")) {
			token = token.replace("Bearer", "").trim();
		}
		PinDTO pin = null;
		User user = userRepo.findUserByToken(token);
		Account account = accountRepo.findByUserId(user.getId());
		double accountBalance = account.getBalance();
		System.out.println("2^^^^^account balance is" + accountBalance);
		if (accountBalance >= topUp.getAmount()) {
			pin = commonUtil.fetchPinDetails(account, user, topUp.getAmount());
		} else {
			throw new InSufficientBalanceException("User does not have the sufficient balance to perform the topUp");
		}
		if (pin != null) {
			accountBalance = accountBalance - topUp.getAmount();
			account.setBalance(accountBalance);
			accountRepo.save(account);
			pin.setAccountBalance(accountBalance);
			commonUtil.addTopUpTransactions(account, topUp);
		}

		System.out.println("2^^^^pin is" + pin);
		return pin;
	}

	@Override
	@Transactional
	public RechargeResponseDTO doRecharge(MobileTopupDTO rechargeRequestDTO, String token) {
		if (token.contains("Bearer")) {
			token = token.replace("Bearer", "").trim();
		}
		User user = userRepo.findUserByToken(token);
		Account account = accountRepo.findByUserId(user.getId());
		double accountBalance = account.getBalance();
		final double rechargeAmount = rechargeRequestDTO.getAmount();
		TransactionResponse transactionResponse = null;
		if (accountBalance >= rechargeAmount) {
			transactionResponse = rechargeClient.processRecharge(rechargeRequestDTO,account);
			System.out.println("2^^^^^^^^transaction response" + transactionResponse);
			if (transactionResponse.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
				accountBalance = accountBalance - rechargeAmount;
				account.setBalance(accountBalance);
				accountRepo.save(account);
				commonUtil.addPrepaidTransactions(account, rechargeRequestDTO, TransactionStatus.SUCCESS,
						transactionResponse);
				return ResponseDTO.builder().code("SOO").message("Recharge Successful").details(transactionResponse)
						.build();

			} else if (!transactionResponse.getCode().equalsIgnoreCase(ResponseStatus.INTERNAL_SERVER_ERROR.getValue())) {
				transactionResponse.setCode(transactionResponse.getCode());
				transactionResponse.setMessage(transactionResponse.getMessage());
				System.out.println("While processRecharge=" + transactionResponse.getMessage() + "trxnId "
						+ transactionResponse.getReferenceId());
				commonUtil.addPrepaidTransactions(account, rechargeRequestDTO, TransactionStatus.FAILED,
						transactionResponse);
			}
		} else {
			throw new InSufficientBalanceException("User does not have the sufficient balance to perform the topUp");
		}
		return ResponseDTO.builder().code(ResponseStatus.FAILURE.getValue()).message("Recharge Failed")
				.details(transactionResponse).build();

	}

}
