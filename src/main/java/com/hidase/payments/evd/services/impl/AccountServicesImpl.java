package com.hidase.payments.evd.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.hidase.payments.evd.dto.AccountDTO;
import com.hidase.payments.evd.dto.UserAccountDetail;
import com.hidase.payments.evd.dto.UserDetail;
import com.hidase.payments.evd.entities.Account;
import com.hidase.payments.evd.entities.AccountMapper;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.entities.Role;
import com.hidase.payments.evd.entities.Status;
import com.hidase.payments.evd.entities.Transactions;
import com.hidase.payments.evd.exception.AccountNotFoundException;
import com.hidase.payments.evd.exception.UserAlreadyExistedException;
import com.hidase.payments.evd.exception.UserNotFoundException;
import com.hidase.payments.evd.repository.AccountMapperRepository;
import com.hidase.payments.evd.repository.AccountRepository;
import com.hidase.payments.evd.repository.RoleRepository;
import com.hidase.payments.evd.repository.TransactionsRepository;
import com.hidase.payments.evd.repository.UserRepository;
import com.hidase.payments.evd.services.AccountServices;
import com.hidase.payments.evd.utilities.CommonUtil;

@Service
public class AccountServicesImpl implements AccountServices {

	@Autowired
	private AccountRepository acctRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private AccountMapperRepository accountMapperRepository;
	@Autowired
	private TransactionsRepository transactionsRepo;

	@Override
	public ResponseEntity<Object> addAccount(AccountDTO account) throws Exception {
		User user = new User();
		if (userRepository.findUserByEmail(account.getEmail()) != null) {
			throw new UserAlreadyExistedException("User is already registered with the provided email");
		}
		if (userRepository.findUserByMobileNumber(account.getMobileNumber()) != null) {
			throw new UserAlreadyExistedException("User is already registered with the provided mobile Number");
		}

		user.setEmail(account.getEmail());
		user.setMobileNumber(account.getMobileNumber());
		user.setFullName(account.getFullName());
		String getpass = CommonUtil.randomAlphaNumeric(10);
		System.out.println("2^^^^user password is"+getpass);
		user.setPassword(CommonUtil.hashPassword(getpass));
		user.setStatus("ACTIVE");
		user.setCreated(CommonUtil.getTimeStamp());
		user.setLastUpdated(CommonUtil.getTimeStamp());
		userRepository.save(user);

		User user1 = userRepository.findUserByMobileNumber(account.getMobileNumber());

		Account acct = new Account();
		acct.setAccountNumber(CommonUtil.generateAccountNumber());
		acct.setCreated(CommonUtil.getTimeStamp());
		acct.setLastUpdated(CommonUtil.getTimeStamp());
		acct.setStatus(Status.ACTIVE);
		acct.setUserId(user1.getId());
		acctRepository.save(acct);

		JSONObject jsb = new JSONObject(account);
		JSONArray jsonArray = jsb.getJSONArray("userAccess");
		HashMap<Integer, String> accessMap = CommonUtil.userAcessMap();

		ArrayList<String> useraccess = new ArrayList<String>();
		for (int i = 0; i < jsonArray.length(); i++) {
			int j = jsonArray.getInt(i);
			useraccess.add(accessMap.get(j));

		}
		String privileges = String.join(",", useraccess);

		Role role = new Role();
		role.setUserAccess(privileges);
		role.setRoleName(account.getRoleName());
		role.setRoleType(account.getRoleType());
		role.setUserId(user1.getId());
		roleRepository.save(role);
		return new ResponseEntity<Object>("Account created successfully", HttpStatus.CREATED);

	}

	@Override
	public UserDetail updateUser(AccountDTO account, long userid) throws Exception {
		UserDetail userDeatil = new UserDetail();
		if (userRepository.existsById(userid)) {
			Optional<User> user = userRepository.findById(userid);
			User userObject = user.get();
			if (!CommonUtil.isNullValue(account.getEmail())) {
				userObject.setEmail(account.getEmail());
				userDeatil.setEmail(account.getEmail());
			}

			if (!CommonUtil.isNullValue(account.getMobileNumber())) {
				userObject.setMobileNumber(account.getMobileNumber());
				userDeatil.setMobileNumber(account.getMobileNumber());
			}

			if (!CommonUtil.isNullValue(account.getFullName())) {
				userObject.setFullName(account.getFullName());
				userDeatil.setFullName(account.getFullName());
			}

			if (account.getUserAccess() != null) {
				JSONObject jsb = new JSONObject(account);
				JSONArray jsonArray = jsb.getJSONArray("userAccess");
				List<Role> accessList = roleRepository.findAll();
				ArrayList<String> useraccess = new ArrayList<>();
				for (int i = 0; i < jsonArray.length(); i++) {
					int j = jsonArray.getInt(i);
					useraccess.add(accessList.get(j - 1).getUserAccess());
				}
				userDeatil.setUserAccess(useraccess);
			}
			userObject.setLastUpdated(CommonUtil.getTimeStamp());
			userRepository.save(userObject);
			Optional<User> user1 = userRepository.findById(userid);
			Account acct =acctRepository.findByUserId(userid);
			User userObject1 = user1.get();
			userDeatil.setAccountBalance(acct.getBalance());
			userDeatil.setCreated(userObject1.getCreated());
			userDeatil.setEmail(userObject1.getEmail());
			userDeatil.setFullName(userObject1.getFullName());
			userDeatil.setId(userObject1.getId());
			userDeatil.setMobileNumber(userObject1.getMobileNumber());
			userDeatil.setUserStatus(userObject1.getStatus());
			userDeatil.setLastUpdated(userObject1.getLastUpdated());
		} else {
			throw new UserNotFoundException("No user is present with provided user id " + userid);
		}
		return userDeatil;

	}

	@Override
	public ResponseEntity<Object> deleteAccount(long accountid) throws Exception {
		if (acctRepository.existsById(accountid)) {
			Optional<Account> acct = acctRepository.findById(accountid);
			Account accountObject = acct.get();
			acctRepository.delete(accountObject);
			System.out.print("deletd success");
		} else {
			throw new AccountNotFoundException(
					"Account with the provided account id is not present " + " " + accountid);
		}
		return new ResponseEntity<Object>("Account Deleted SucessFully", HttpStatus.OK);

	}

	@Override
	public ResponseEntity<Object> deleteUser(long userid) throws Exception {
		if (userRepository.existsById(userid)) {
			Optional<User> userAcct = userRepository.findById(userid);
			User userObject = userAcct.get();
			userRepository.delete(userObject);
		} else {
			throw new UserNotFoundException("No user is present with provided user id " + userid);
		}
		return new ResponseEntity<Object>("User Deleted SucessFully", HttpStatus.OK);
	}

	@Override
	public List<Account> getAccounts() throws Exception {

		List<Account> acct = acctRepository.findAll();
		return acct;
	}

	@Override
	public List<UserDetail> getUsers() throws Exception {
		List<User> userList = userRepository.findAll();
		List<UserDetail> userdetailList = new ArrayList<>();
		for (int i = 0; i < userList.size(); i++) {
			UserDetail detail = new UserDetail();
			User user = userList.get(i);
			detail.setEmail(user.getEmail());
			detail.setMobileNumber(user.getMobileNumber());
			detail.setUserStatus(user.getStatus());
			detail.setId((int) user.getId());
			detail.setCreated(user.getCreated());
			userdetailList.add(detail);
		}
		return userdetailList;
	}

	@Override
	public Account getAccount(long accountid) throws Exception {
		Account accountObject = null;
		if (acctRepository.existsById(accountid)) {
			Optional<Account> acct = acctRepository.findById(accountid);
			accountObject = acct.get();

		} else {
			throw new AccountNotFoundException(
					"Account with the provided account id is not present " + " " + accountid);
		}
		return accountObject;
	}

	@Override
	public List<UserAccountDetail> getAccountsUser(long accountid) throws Exception {
		Account accountObject = null;
		List<UserAccountDetail> list = new ArrayList<>();
		if (acctRepository.existsById(accountid)) {

			Optional<Account> acct = acctRepository.findById(accountid);
			accountObject = acct.get();
			long userId = accountObject.getUserId();
			Optional<User> userObject = userRepository.findById(userId);
			User user = userObject.get();
			List<AccountMapper> accountList = findAccountsById(accountid);
			UserAccountDetail detail = new UserAccountDetail();
			detail.setAccountId(accountid);
			detail.setUserId(userId);
			detail.setAccountNumber(accountObject.getAccountNumber());
			detail.setBalance(accountObject.getBalance());
			detail.setAccountStatus(accountObject.getStatus());
			if (!CommonUtil.isNullValue(user.getMobileNumber())) {
				detail.setMobileNumber(user.getMobileNumber());
			}
			if (!CommonUtil.isNullValue(user.getEmail())) {
				detail.setEmail(user.getEmail());
			}
			detail.setUserStatus(user.getStatus());
			list.add(detail);
			for (int i = 0; i < accountList.size(); i++) {
				UserAccountDetail det = new UserAccountDetail();
				long acctId = accountList.get(i).getSecondaryAccountId();
				Optional<Account> accountObj = acctRepository.findById(acctId);
				Account act = accountObj.get();
				long uid = act.getUserId();
				Optional<User> userObj = userRepository.findById(uid);
				User user1 = userObj.get();
				det.setAccountId(act.getId());
				det.setAccountNumber(act.getAccountNumber());
				det.setAccountStatus(act.getStatus());
				det.setBalance(act.getBalance());
				det.setEmail(user1.getEmail());
				det.setMobileNumber(user1.getMobileNumber());
				det.setUserId(user1.getId());
				det.setUserStatus(user1.getStatus());
				list.add(det);
			}

		} else {
			throw new UserNotFoundException("No users are present for a provided account id " + " " + accountid);
		}
		return list;
	}

	@Override
	public ResponseEntity<Object> addUserAccount(AccountDTO account, long accountid) throws Exception {

		User userobject = new User();
		if (acctRepository.existsById(accountid)) {

			userobject.setEmail(account.getEmail());
			userobject.setMobileNumber(account.getMobileNumber());
			String getpass = CommonUtil.randomAlphaNumeric(10);
			userobject.setPassword(CommonUtil.hashPassword(getpass));
			userobject.setStatus("ACTIVE");
			userobject.setCreated(CommonUtil.getTimeStamp());
			userobject.setLastUpdated(CommonUtil.getTimeStamp());
			userRepository.save(userobject);
			User user1 = userRepository.findUserByMobileNumber(account.getMobileNumber());
			JSONObject jsb = new JSONObject(account);
			JSONArray jsonArray = jsb.getJSONArray("userAccess");
			HashMap<Integer, String> accessMap = CommonUtil.userAcessMap();
			ArrayList<String> useraccess = new ArrayList<String>();
			for (int i = 0; i < jsonArray.length(); i++) {
				int j = jsonArray.getInt(i);
				useraccess.add(accessMap.get(j));
			}
			String privileges = String.join(",", useraccess);
			Role role = new Role();
			role.setUserAccess(privileges);
			role.setRoleName(account.getRoleName());
			role.setRoleType(account.getRoleType());
			role.setUserId(user1.getId());
			roleRepository.save(role);
			Account acct = new Account();
			String accountNumber = CommonUtil.generateAccountNumber();
			acct.setAccountNumber(accountNumber);
			acct.setBalance(0.00);
			acct.setCreated(CommonUtil.getTimeStamp());
			acct.setLastUpdated(CommonUtil.getTimeStamp());
			acct.setStatus(Status.ACTIVE);
			acct.setUserId(user1.getId());
			acctRepository.save(acct);

			Optional<Account> acount = acctRepository.findById(accountid);
			Account acount1 = acctRepository.findByAccountNumber(accountNumber);
			Account accountObject = acount.get();
			AccountMapper mapper = new AccountMapper();
			mapper.setPrimaryAccountId(accountObject.getId());
			mapper.setSecondaryAccountId(acount1.getId());
			accountMapperRepository.save(mapper);
		} else {
			throw new AccountNotFoundException("No account is present for a provided account id" + accountid);
		}
		return new ResponseEntity<Object>("User added successfully to the account", HttpStatus.CREATED);

	}

	@Override
	public User findUserByEmail(String email) throws Exception {
		// TODO Auto-generated method stub
		User user = userRepository.findUserByEmail(email);
		return user;
	}

	@Override
	public UserDetail getUser(long userid) throws Exception {
		User userObject = null;
		UserDetail detail = new UserDetail();
		if (userRepository.existsById(userid)) {
			Optional<User> user = userRepository.findById(userid);
			userObject = user.get();
			Account acct = acctRepository.findByUserId(userObject.getId());
			detail.setId(userObject.getId());
			detail.setEmail(userObject.getEmail());
			detail.setMobileNumber(userObject.getMobileNumber());
			detail.setUserStatus(userObject.getStatus());
			detail.setCreated(userObject.getCreated());
			detail.setFullName(userObject.getFullName());
			detail.setAccountBalance(acct.getBalance());

		} else {
			throw new UserNotFoundException("User with provided user id is not present" + " " + userid);
		}
		return detail;
	}

	@Override
	public User findUserByToken(String token) throws Exception {
		User user = userRepository.findUserByToken(token);
		return user;
	}

	@Override
	public Role findUserByUserid(long userid) throws Exception {
		Role role = roleRepository.findUserByUserId(userid);
		return role;
	}

	@Override
	public List<AccountMapper> findAccountsById(long primaryaccountid) throws Exception {
		List<AccountMapper> list = accountMapperRepository.findAccountsByprimaryAccountId(primaryaccountid);

		return list;
	}

	@Override
	public List<Transactions> getTransactions(long accountId) throws Exception {
		List<Transactions> trans = transactionsRepo.findTransactionsByAccountId(accountId);
		return trans;
	}

	@Override
	public List<Transactions> getPrefundTransactions(long accountId) {
		List<Transactions> prefundTrans = transactionsRepo.findTypeTransactions(accountId, "PREFUND_TRANSACTION");
		return prefundTrans;
	}

	@Override
	public List<Transactions> getTopUpTransactions(long accountId) {
		List<Transactions> topUpTrans = transactionsRepo.findTypeTransactions(accountId, "TOP_UP_TRANSACTION");
		return topUpTrans;
	}

}
