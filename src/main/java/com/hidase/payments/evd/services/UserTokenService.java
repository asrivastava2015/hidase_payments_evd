package com.hidase.payments.evd.services;

import com.hidase.payments.evd.entities.UserToken;

public interface UserTokenService {

    boolean checkTokenExpiry(UserToken token);

    UserToken getToken(long userId);

    UserToken createOrUpdateUserToken(long userId);
}