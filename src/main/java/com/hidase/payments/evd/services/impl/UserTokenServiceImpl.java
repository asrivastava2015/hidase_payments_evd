package com.hidase.payments.evd.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.entities.UserToken;
import com.hidase.payments.evd.repository.UserRepository;
import com.hidase.payments.evd.repository.UserTokenRepository;
import com.hidase.payments.evd.services.UserTokenService;
import com.hidase.payments.evd.utilities.CommonUtil;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserTokenServiceImpl implements UserTokenService {

	private final UserTokenRepository userTokenRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	public UserTokenServiceImpl(final UserTokenRepository userTokenRepository) {
		this.userTokenRepository = userTokenRepository;
	}

	@Override
	public boolean checkTokenExpiry(UserToken token) {

		if (token.getTokenExpiry().before(CommonUtil.getTimeStamp())) {
			return true;
		}

		return false;
	}

	@Override
	public UserToken getToken(long userId) {
		return null;
	}

	@Override
	public UserToken createOrUpdateUserToken(long userId) {
		final UserToken userToken = userTokenRepository.findByUserId(userId);
		System.out.println("2^^^^user token is" + userToken);
		if (Objects.isNull(userToken)) {
			return createNewToken(userId);
		}
		return updateToken(userToken, userId);
	}

	private UserToken updateToken(UserToken userToken, long userId) {
		String token = UUID.randomUUID().toString();
		Optional<User> userObj = userRepository.findById(userId);
		User user = userObj.get();
		user.setToken(token);
		Timestamp time=CommonUtil.getTokenExpiry();
		user.setTokenExpiry(time);
		userRepository.save(user);
		userToken.setTokenExpiry(time);
		userToken.setToken(token);
		return userTokenRepository.save(userToken);
	}

	private UserToken createNewToken(long userId) {
		String token = UUID.randomUUID().toString();
		Optional<User> userObj = userRepository.findById(userId);
		User user = userObj.get();
		user.setToken(token);
		Timestamp time=CommonUtil.getTokenExpiry();
		user.setTokenExpiry(time);
		userRepository.save(user);
		return userTokenRepository.save(UserToken.builder().mfaRequired(false).tokenExpiry(time)
				.userId(userId).token(token).build());
	}
}
