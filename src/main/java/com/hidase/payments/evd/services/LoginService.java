package com.hidase.payments.evd.services;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import com.hidase.payments.evd.dto.AccountDTO;
import com.hidase.payments.evd.dto.LoginResponseDTO;
import com.hidase.payments.evd.dto.PasswordDTO;
import com.hidase.payments.evd.entities.User;

public interface LoginService {

    ResponseEntity<LoginResponseDTO> createOrUpdateSessionToken(String authorization) throws Exception;
    
    ResponseEntity<Object> forgotPassword(AccountDTO account,HttpServletRequest request,User user) throws UnsupportedEncodingException, MessagingException;

	ResponseEntity<Object> updatePassword(User user,PasswordDTO password);

	ResponseEntity<Object>  resetPassword(User user, PasswordDTO password);

	ResponseEntity<Object> errorMessage();

	ResponseEntity<Object> logOut(User user);
}
