package com.hidase.payments.evd.services;

import org.springframework.mail.SimpleMailMessage;

public interface EmailServices {

	public void sendEmail(SimpleMailMessage email);
}
