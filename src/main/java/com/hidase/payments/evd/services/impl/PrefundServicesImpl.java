package com.hidase.payments.evd.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hidase.payments.evd.dto.PrefundActionDTO;
import com.hidase.payments.evd.dto.PrefundDTO;
import com.hidase.payments.evd.entities.Account;
import com.hidase.payments.evd.entities.AccountMapper;
import com.hidase.payments.evd.entities.Prefund;
import com.hidase.payments.evd.entities.PrefundStatus;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.exception.InSufficientBalanceException;
import com.hidase.payments.evd.exception.IncorrectStatusTypeException;
import com.hidase.payments.evd.exception.IncorrectTransactionException;
import com.hidase.payments.evd.exception.PrefundNotFoundException;
import com.hidase.payments.evd.repository.AccountMapperRepository;
import com.hidase.payments.evd.repository.AccountRepository;
import com.hidase.payments.evd.repository.PrefundRepository;
import com.hidase.payments.evd.services.PrefundServices;
import com.hidase.payments.evd.utilities.CommonUtil;

@Service
public class PrefundServicesImpl implements PrefundServices {

	@Autowired
	private PrefundRepository prefundRepository;

	@Autowired
	private AccountRepository acctRepo;

	@Autowired
	private AccountMapperRepository acctMapRepo;

	@Autowired
	private CommonUtil commonUtil;

	@Override
	public PrefundDTO createPrefund(PrefundDTO prefund, User user) throws Exception {
		Prefund pre = new Prefund();
		pre.setAmount(prefund.getAmount());
		String transRefId = user.getId() + CommonUtil.generateTransactionId();
		pre.setTransactionId(transRefId);
		if (!CommonUtil.isNullValue(prefund.getComment())) {
			pre.setComment(prefund.getComment());
		}

		String sysTraceId = Long.toString(CommonUtil.getEpoc()) + user.getId();
		pre.setSystemTraceId(sysTraceId);
		pre.setStatus(PrefundStatus.CREATED);
		pre.setCreated(CommonUtil.getTimeStamp());
		pre.setLastUpdated(CommonUtil.getTimeStamp());
		pre.setUserId(user.getId());
		Account acct = acctRepo.findByUserId((int) user.getId());
		pre.setAccountId(acct.getId());
		prefundRepository.save(pre);
		prefund.setTransactionRefId(transRefId);
		prefund.setSystemTraceId(sysTraceId);
		prefund.setMessage("Prefund Created Successfully");
		return prefund;

	}

	@Override
	public Prefund getPrefund(long prefundId) throws Exception {
		Prefund prefundObject = null;

		if (prefundRepository.existsById(prefundId)) {
			Optional<Prefund> prefund = prefundRepository.findById(prefundId);
			prefundObject = prefund.get();
		} else {
			throw new PrefundNotFoundException("Prefund is not present with the provided prefund id" + " " + prefundId);
		}

		return prefundObject;
	}

	@Override
	public List<Prefund> getUserPrefunds(long userId) throws Exception {

		List<Prefund> pre = findPrefundByUserId(userId);

		if (pre.size() == 0) {
			throw new PrefundNotFoundException("No prefund is present for a user with provided user id" + " " + userId);
		}
		return pre;
	}

	@Override
	public List<Prefund> getPrefunds() throws Exception {

		return prefundRepository.findAll();
	}

	@Override
	public List<Prefund> getAccountPrefunds(long accountId) throws Exception {
		List<Prefund> pre = findPrefundByAccountId(accountId);
		if (pre.size() == 0) {
			throw new PrefundNotFoundException(
					"No prefund is present for the account with provided accountId" + " " + accountId);
		}
		return pre;
	}

	@Override
	public List<Prefund> findPrefundByUserId(long userId) throws Exception {

		List<Prefund> prefunds = prefundRepository.findPrefundByUserId(userId);
		return prefunds;

	}

	@Override
	public List<Prefund> findPrefundByAccountId(long accountId) throws Exception {
		List<Prefund> prefunds = prefundRepository.findPrefundByAccountId(accountId);
		return prefunds;
	}

	@Override
	public Prefund prefundAction(PrefundActionDTO prefund, long prefundId, long primaryAccountId) throws Exception {
		Prefund prefundDetail = null;
		if (prefundRepository.existsById(prefundId)) {
			Optional<Prefund> pre = prefundRepository.findById(prefundId);
			prefundDetail = pre.get();
			String tranansactionId = prefundDetail.getTransactionId();
			String sysTrace = prefundDetail.getSystemTraceId();
			if (tranansactionId.equals(prefund.getTransactionId()) && sysTrace.equals(prefund.getSystemTraceId())) {

				String status = prefund.getStatus();
				if (status.toLowerCase().equals("approved")) {
					Optional<Account> primaryAccountObj = acctRepo.findById(primaryAccountId);
					Account primaryAccont = primaryAccountObj.get();
					double primaryBalance = primaryAccont.getBalance();
					if (primaryBalance < Double.parseDouble(prefundDetail.getAmount())) {
						throw new InSufficientBalanceException(
								"Sufficient Balance is not present in the account to approve the prefund");
					}
					primaryBalance = primaryBalance - Double.parseDouble(prefundDetail.getAmount());
					primaryAccont.setBalance(primaryBalance);
					primaryAccont.setLastUpdated(CommonUtil.getTimeStamp());
					Optional<Account> acctObj = acctRepo.findById(prefundDetail.getAccountId());
					Account acct = acctObj.get();
					double balance = acct.getBalance();
					balance = balance + Double.parseDouble(prefundDetail.getAmount());
					acct.setBalance(balance);
					acct.setLastUpdated(CommonUtil.getTimeStamp());
					acctRepo.save(acct);
					acctRepo.save(primaryAccont);
					prefundDetail.setStatus(PrefundStatus.APPROVED);
					prefundDetail.setLastUpdated(CommonUtil.getTimeStamp());
					commonUtil.addPrefundTransactions(primaryAccont, acct, prefundDetail);

				} else if (status.toLowerCase().equals("declined")) {
					prefundDetail.setStatus(PrefundStatus.DECLINED);
					prefundDetail.setLastUpdated(CommonUtil.getTimeStamp());
				} else {
					throw new IncorrectStatusTypeException("Incorrect status type");
				}
				prefundRepository.save(prefundDetail);
			} else {
				throw new IncorrectTransactionException("Incorrect transaction data");
			}
		} else {
			throw new PrefundNotFoundException("No prefund is present with the provided prefund id" + " " + prefundId);
		}
		return prefundDetail;
	}

	@Override
	public List<List<Prefund>> getPrimaryAccountPrefunds(long accountId) {
		List<AccountMapper> acctMap = acctMapRepo.findAccountsByprimaryAccountId(accountId);
		List<List<Prefund>> finalList = new ArrayList<>();
		for (int i = 0; i < acctMap.size(); i++) {
			List<Prefund> prefundList = prefundRepository
					.findPrefundByAccountId(acctMap.get(i).getSecondaryAccountId());
			if (!prefundList.isEmpty()) {
				finalList.add(prefundList);
			}
		}
		if (finalList.isEmpty()) {
			throw new PrefundNotFoundException(
					"No prefunds are raised against this account.Please provide a correct accountId");
		}
		return finalList;
	}

}
