package com.hidase.payments.evd.services;

import java.util.List;
import org.springframework.http.ResponseEntity;
import com.hidase.payments.evd.dto.AccountDTO;
import com.hidase.payments.evd.dto.UserAccountDetail;
import com.hidase.payments.evd.dto.UserDetail;
import com.hidase.payments.evd.entities.Account;
import com.hidase.payments.evd.entities.AccountMapper;
import com.hidase.payments.evd.entities.Role;
import com.hidase.payments.evd.entities.Transactions;
import com.hidase.payments.evd.entities.User;

public interface AccountServices {

	ResponseEntity<Object> addAccount(AccountDTO account) throws Exception;
	ResponseEntity<Object> deleteAccount(long accountid) throws Exception;
	Account getAccount(long accountid) throws Exception;
	List<UserAccountDetail> getAccountsUser(long accountid) throws Exception;
	List<Account> getAccounts() throws Exception;
	
	UserDetail updateUser(AccountDTO account,long userid) throws Exception;
	ResponseEntity<Object> deleteUser(long userid) throws Exception;
	ResponseEntity<Object> addUserAccount(AccountDTO account,long accountid) throws Exception;
	UserDetail getUser(long userid) throws Exception;
	
	List<UserDetail> getUsers() throws Exception;
	User findUserByEmail(String email) throws Exception;
	User findUserByToken(String token) throws Exception;
	Role findUserByUserid(long userid) throws Exception;
	List<AccountMapper> findAccountsById(long primaryaccountid) throws Exception;
	List<Transactions> getTransactions(long accountId) throws Exception;
	List<Transactions> getPrefundTransactions(long accountId);
	List<Transactions> getTopUpTransactions(long accountId);
	
}
