package com.hidase.payments.evd.services;

import java.util.List;
import com.hidase.payments.evd.dto.PrefundActionDTO;
import com.hidase.payments.evd.dto.PrefundDTO;
import com.hidase.payments.evd.entities.Prefund;
import com.hidase.payments.evd.entities.User;

public interface PrefundServices {

	PrefundDTO createPrefund(PrefundDTO prefund,User user) throws Exception;
	Prefund getPrefund(long prefundId) throws Exception;
	List<Prefund> getUserPrefunds(long userId) throws Exception;
	List<Prefund> getAccountPrefunds(long accountId) throws Exception;
	List<Prefund> getPrefunds() throws Exception;
	List<Prefund> findPrefundByUserId(long userId) throws Exception;
	List<Prefund> findPrefundByAccountId(long accountId) throws Exception;
	Prefund prefundAction(PrefundActionDTO prefund,long prefundId,long primaryAccountId) throws Exception;
	List<List<Prefund>> getPrimaryAccountPrefunds(long accountId);
	
}
