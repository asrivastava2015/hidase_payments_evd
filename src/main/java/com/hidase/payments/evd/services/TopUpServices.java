package com.hidase.payments.evd.services;

import org.springframework.stereotype.Service;

import com.hidase.payments.evd.dto.MobileTopupDTO;
import com.hidase.payments.evd.dto.PinDTO;
import com.hidase.payments.evd.dto.RechargeResponseDTO;
import com.hidase.payments.evd.dto.TopUpDTO;


@Service
public interface TopUpServices {

	PinDTO topUp(TopUpDTO topUp,String token) throws Exception;
	RechargeResponseDTO doRecharge(MobileTopupDTO rechargeRequestDTO, String token);
	
}
