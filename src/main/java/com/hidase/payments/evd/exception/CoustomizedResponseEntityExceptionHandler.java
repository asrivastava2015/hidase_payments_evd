package com.hidase.payments.evd.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestController
public class CoustomizedResponseEntityExceptionHandler  {
	@ExceptionHandler(value=Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request){
		 ExceptionResponse exception=new ExceptionResponse(new Date(),ex.getMessage(),request.getDescription(false));
		 
		 return new ResponseEntity<Object>(exception,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value=UserNotFoundException.class)
	public final ResponseEntity<Object> handleUserNotFoundException(Exception ex, WebRequest request){
		 ExceptionResponse exception=new ExceptionResponse(new Date(),ex.getMessage(),request.getDescription(false));
		 
		 return new ResponseEntity<Object>(exception,HttpStatus.NOT_FOUND);
	}

}
