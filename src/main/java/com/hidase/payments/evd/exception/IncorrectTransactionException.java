package com.hidase.payments.evd.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IncorrectTransactionException extends RuntimeException{

	public IncorrectTransactionException(String message) {
		super(message);
	}
}
