package com.hidase.payments.evd.exception;

public class PasswordExpiredException extends RuntimeException {
	
	public PasswordExpiredException(String message) {
		super(message);
	}

}
