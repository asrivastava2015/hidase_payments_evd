package com.hidase.payments.evd.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidSessionException extends RuntimeException {

	public InvalidSessionException(String message) {
		super(message);
	}
}
