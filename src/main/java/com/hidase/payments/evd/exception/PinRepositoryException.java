package com.hidase.payments.evd.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PinRepositoryException extends RuntimeException{
	public PinRepositoryException(String message) {
		super(message);
	}
}
