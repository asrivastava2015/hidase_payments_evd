package com.hidase.payments.evd.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AccountNotFoundException extends RuntimeException {

	
	public AccountNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
