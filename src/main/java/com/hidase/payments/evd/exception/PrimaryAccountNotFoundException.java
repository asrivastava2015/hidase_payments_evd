package com.hidase.payments.evd.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PrimaryAccountNotFoundException extends RuntimeException {
	
	public PrimaryAccountNotFoundException(String message) {
		super(message);
	}
}
