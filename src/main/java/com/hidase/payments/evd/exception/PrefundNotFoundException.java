package com.hidase.payments.evd.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PrefundNotFoundException extends RuntimeException{
	
	public PrefundNotFoundException(String message) {
		super(message);
	}

}
