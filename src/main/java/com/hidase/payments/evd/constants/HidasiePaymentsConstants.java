package com.hidase.payments.evd.constants;

public class HidasiePaymentsConstants {

	public static final String CREATEACCOUNTANDUSER="createAccountAndUser";
	public static final String RETRIEVEALL="retrieveAll";
	public static final String UPDATEUSER="updateUser";
	public static final String DELETEUSER="deleteUser";
	public static final String DELETEACCOUNT="deleteAccount";
	public static final String PREFUNDREQUEST="prefundRequest";
	public static final String PREFUNDACTION="prefundAction";
	public static final String RETRIEVEASSOCIATEDUSERS="retrieveAssociatedUsers";
	public static final String RETRIEVEASSOCIATEDPREFUNDS="retrieveAssociatedPrefunds";
	public static final String CREATEUSEREXISTINGACCOUNT="createUserExistingAccount";
	public static final String RETRIEVE="retrieve";
	public static final String TOPUP="topUp";
	public static final String RETRIEVEALLPREFUNDS = "retrieveAllprefuds";
	public static final String DEBIT="debit";
	public static final String CREDIT = "credit";
	                                                
	
	
	
}
