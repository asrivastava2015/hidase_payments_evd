package com.hidase.payments.evd.constants;

public class RechargeConstant {

	public static final String PREPAID_CODE = "EXRCTRFREQ";

	public static final String POSTPAID_CODE = "EXPPBREQ";

	public static final String LANGUAGE_EN = "0";

	public static final String LANGUAGE_AMHARIC = "1";

	public static final int SELECTOR_RECHARGE = 1;

	public static final int SELECTOR_BILLPAYMENT = 0;

	public static final String EXTNWCODE = "ET";

	public static final String EXTCODE = "123";
	public static final String PIN = "8102";
	public static final String MSISDN = "904167961";
	
	public static final String PREPAID = "Prepaid";
	public static final String POSTPAID = "Postpaid";
	public static final String RECHARGE_STATUS_SUCCESS = "200";
	public static final String RECHARGE_STATUS_FAIL = "206";
	public static final String RECHARGE_STATUS_IN_PROCESS = "205";
	public static final String RECHARGE_STATUS_AMBIGOUS = "250";
	
	public static final String HIDASE_URL = "https://10.208.254.131/";
	public static final String HIDASE_LOGIN = "hidase";
	public static final String HIDASE_PASSWORD = "a8147bc5f168bc24";
	
	public static final String COMMON_REQUEST = "?LOGIN=abyssinia01&PASSWORD=c8a4ba359067e473&REQUEST_GATEWAY_CODE=abyssinia&REQUEST_GATEWAY_TYPE=EXTGW&SERVICE_PORT=190&SOURCE_TYPE=EXTGW";
	public static final String C2SRECEIVER = "pretups/C2SReceiver";
	public static final String CHECK_RECHARGE_STATUS = "pretups/C2SReceiver";
	
	//public static final String C2SRECEIVER = "pretups/C2SReceiver?LOGIN=hidase&PASSWORD=a8147bc5f168bc24&REQUEST_GATEWAY_CODE=hidase01&REQUEST_GATEWAY_TYPE=EXTGW&SERVICE_PORT=190&SOURCE_TYPE=EXTSYS";
	
}
