package com.hidase.payments.evd.auth;

import com.hidase.payments.evd.entities.Role;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.exception.InvalidSessionException;
import com.hidase.payments.evd.exception.PrivilegedException;
import com.hidase.payments.evd.exception.UserNotRegisteredException;
import com.hidase.payments.evd.services.AccountServices;
import com.hidase.payments.evd.utilities.CommonUtil;

public class BasicAuth {

	private boolean isPrivileged = false;

	public void isUserAuthenticated(String authString, AccountServices accountService, String action) throws Exception {
		if(authString.contains("Bearer")) {
			authString=authString.replace("Bearer", "").trim();
		}
		System.out.println("2^^^^auth string is"+authString);
		User user = accountService.findUserByToken(authString);
		if (user == null) {
			throw new UserNotRegisteredException("User is not registered.Please register the user");
		}
		if (!user.getToken().equals(authString)) {
			throw new InvalidSessionException("User session is expired");
		}
		System.out.println("2^^^^^^^^checking the expiry"+user.getTokenExpiry());
		System.out.println("2^^^^^^^^checking the current time"+CommonUtil.getTimeStamp());
		System.out.println("2^^^^^^^^checking if true condition"+user.getTokenExpiry().before(CommonUtil.getTimeStamp()));
		if (user.getTokenExpiry().before(CommonUtil.getTimeStamp())) {
			throw new InvalidSessionException("User session is expired");
		}
		// TO DO
		Role role = accountService.findUserByUserid(user.getId());
		String userAccess = role.getUserAccess();
		String[] privileges = userAccess.split(",");
		for (int i = 0; i < privileges.length; i++) {
			if (privileges[i].equals(action)) {
				isPrivileged = true;
			}
		}

		if (!isPrivileged) {
			throw new PrivilegedException("You does not have the required Privileges to perform this action");
		}

	}

	public User isUserAuthenticate(String authString, AccountServices accountService, String action) throws Exception {
		if(authString.contains("Bearer")) {
			authString=authString.replace("Bearer", "").trim();
		}
		User user = accountService.findUserByToken(authString);

		if (user == null) {
			throw new UserNotRegisteredException("User is not registered.Please register the user");
		}

		if (!user.getToken().equals(authString)) {
			throw new InvalidSessionException("User session is expired");
		}

		if (user.getTokenExpiry().before(CommonUtil.getTimeStamp())) {
			throw new InvalidSessionException("User session is expired");
		}
		// TO DO
		Role role = accountService.findUserByUserid(user.getId());
		String userAccess = role.getUserAccess();
		String[] privileges = userAccess.split(",");

		for (int i = 0; i < privileges.length; i++) {

			if (privileges[i].equals(action)) {
				isPrivileged = true;
			}
		}

		if (!isPrivileged) {
			throw new PrivilegedException("You does not have the required Privileges to perform this action");
		}

		return user;
	}

}
