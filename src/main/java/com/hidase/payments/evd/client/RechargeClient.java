package com.hidase.payments.evd.client;

import com.hidase.payments.evd.constants.RechargeConstant;
import com.hidase.payments.evd.dto.MobileTopupDTO;
import com.hidase.payments.evd.entities.Account;
import com.hidase.payments.evd.entities.TransactionStatus;
import com.hidase.payments.evd.utilities.CommonUtil;
import com.hidase.payments.evd.utilities.XMLBuilder;
import com.hidase.payments.evd.utilities.XMLParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;


import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@Component
@Slf4j
public class RechargeClient {

	@Autowired
	private CommonUtil commonUtil;
    
    public TransactionResponse processRecharge(MobileTopupDTO req,Account account) {

    	CommonUtil.setTransactionId(req);
    	System.out.println("2^^^^mobile trans"+req.getReferenceId());
		TransactionResponse result = new TransactionResponse();
		try {
			Client client = createClient();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					RechargeConstant.HIDASE_URL + RechargeConstant.C2SRECEIVER + RechargeConstant.COMMON_REQUEST);
			ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_XML_VALUE).post(ClientResponse.class,
					XMLBuilder.rechargeRequest(req));
			String strResponse = clientResponse.getEntity(String.class);
			result = XMLParser.parseRechargeResponse(strResponse);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Unable to process Recharege request now! Please try after some time");
			result.setReferenceId(req.getReferenceId());
			System.out.println("While processRecharge="+result.getMessage()+"trxnId "+result.getReferenceId());
			commonUtil.addPrepaidTransactions(account, req, TransactionStatus.FAILED,result);
		
		}
		return result;
	
    }
    
    public static Client createClient() throws Exception {
		Client restClient = null;
		try {
			Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				@Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
						throws CertificateException {
					// TODO Auto-generated method stub

				}

				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
						throws CertificateException {
					// TODO Auto-generated method stub

				}

				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			} };

			// Ignore differences between given hostname and certificate
			// hostname
			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			ClientConfig config = new DefaultClientConfig();
			SSLContext sc = SSLContext.getInstance("TLSv1.2"); //$NON-NLS-1$
			sc.init(null, trustAllCerts, new SecureRandom());
			sc.init(null, trustAllCerts, new SecureRandom());
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
			config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hv, sc));
			restClient = Client.create(config);
			System.setProperty("javax.net.debug", "ssl");
		} catch (KeyManagementException e) {
			String errorMsg = "client initialization error: " //$NON-NLS-1$
					+ e.getMessage();
			System.err.println(errorMsg);
			throw new Exception(errorMsg, e);
		}
		return restClient;
	}
    
   

}
