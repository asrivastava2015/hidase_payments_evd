package com.hidase.payments.evd.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionResponse {

    private boolean success;
    public String code;
    public String message;
    private String extReferenceId;
    private String referenceId;
    private boolean pending;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getExtReferenceId() {
		return extReferenceId;
	}
	public void setExtrReferenceId(String extReferenceId) {
		this.extReferenceId = extReferenceId;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public boolean isPending() {
		return pending;
	}
	public void setPending(boolean pending) {
		this.pending = pending;
	}
	@Override
	public String toString() {
		return "TransactionResponse [success=" + success + ", code=" + code + ", message=" + message
				+ ", extrReferenceId=" + extReferenceId + ", referenceId=" + referenceId + ", pending=" + pending
				+ "]";
	}

    

}
