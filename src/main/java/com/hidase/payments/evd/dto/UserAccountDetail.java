package com.hidase.payments.evd.dto;

import com.hidase.payments.evd.entities.Status;

public class UserAccountDetail {

	private long userId;
	private long accountId;
	private String email;
	private String mobileNumber;
	private String accountNumber;
	private double balance;
	private String userStatus;
	private Status accountStatus;
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public Status getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(Status accountStatus) {
		this.accountStatus = accountStatus;
	}
	@Override
	public String toString() {
		return "UserAccountDetail [userId=" + userId + ", accountId=" + accountId + ", email=" + email
				+ ", mobileNumber=" + mobileNumber + ", accountNumber=" + accountNumber + ", balance=" + balance
				+ ", userStatus=" + userStatus + ", accountStatus=" + accountStatus + "]";
	}
	
	
}
