package com.hidase.payments.evd.dto;

import com.hidase.payments.evd.constants.RechargeConstant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class MobileTopupDTO implements RechargeRequestDTO {

    public static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");


    private String type;
    private String serviceType;
    private String referenceId;
    private String mobileNumber;
    private double amount;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }


    public String getType() {
        return type;
    }


	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "MobileTopupDTO [type=" + type + ", serviceType=" + serviceType + ", referenceId=" + referenceId
				+ ", mobileNumber=" + mobileNumber + ", amount=" + amount + "]";
	}

	public Date getCurrentDate() {
        Date date = new Date();
        try {
            String d = formatter.format(date);
            System.out.println("d==" + d);
            Date d3 = formatter.parse(d);
            Calendar calender = Calendar.getInstance();
            calender.setTime(d3);
            long m = calender.getTimeInMillis();
            calender.setTimeInMillis(m);
            Date txdate = calender.getTime();
            System.out.println("date==" + txdate);
            return txdate;
            //		date = formatter.parse(d);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getCurrentDate1() {
        Date date = new Date();
        String d;
        try {
            d = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return d;
    }

}
