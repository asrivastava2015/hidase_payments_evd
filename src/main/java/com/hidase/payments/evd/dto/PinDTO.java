package com.hidase.payments.evd.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PinDTO {

	private long id;
	private double faceValue;
	private String pin;
	private String serial;
	@JsonFormat(pattern = "yyyyMMdd")
	private Date expiry;
	private double accountBalance;
	public double getFaceValue() {
		return faceValue;
	}
	public void setFaceValue(double faceValue) {
		this.faceValue = faceValue;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public Date getExpiry() {
		return expiry;
	}
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	@Override
	public String toString() {
		return "PinDTO [id=" + id + ", faceValue=" + faceValue + ", pin=" + pin + ", serial=" + serial + ", expiry="
				+ expiry + ", accountBalance=" + accountBalance + "]";
	}
	
}
