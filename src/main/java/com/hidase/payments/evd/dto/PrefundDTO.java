package com.hidase.payments.evd.dto;



public class PrefundDTO {

	private String amount;
	private String transactionRefId;
	private String systemTraceId;
	private String comment;
	private String message;
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getTransactionRefId() {
		return transactionRefId;
	}
	public void setTransactionRefId(String transactionRefId) {
		this.transactionRefId = transactionRefId;
	}

	
	public String getSystemTraceId() {
		return systemTraceId;
	}
	public void setSystemTraceId(String systemTraceId) {
		this.systemTraceId = systemTraceId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "PrefundDTO [amount=" + amount + ", transactionRefId=" + transactionRefId + ", systemTraceId="
				+ systemTraceId + ", comment=" + comment + ", message=" + message + "]";
	}
	
}
