package com.hidase.payments.evd.dto;

import java.util.Arrays;

public class AccountDTO {

	private String email;
	private String mobileNumber;
	private String roleName;
	private String roleType;
	private String[] userAccess;
	private String fullName;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	public String[] getUserAccess() {
		return userAccess;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public void setUserAccess(String[] userAccess) {
		this.userAccess = userAccess;
	}
	@Override
	public String toString() {
		return "AccountDTO [email=" + email + ", mobileNumber=" + mobileNumber + ", roleName=" + roleName
				+ ", roleType=" + roleType + ", userAccess=" + Arrays.toString(userAccess) + ", fullName=" + fullName
				+ "]";
	}
}
