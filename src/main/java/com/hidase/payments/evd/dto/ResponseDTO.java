package com.hidase.payments.evd.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseDTO implements RechargeResponseDTO {

    private String code;
    private String message;
    private Object details;


}
