package com.hidase.payments.evd.dto;

import java.sql.Timestamp;
import java.util.ArrayList;


public class UserDetail {
	private long id;
	private String email;
	private String mobileNumber;
	private String userStatus;
	private String fullName;
	private double accountBalance;
	private ArrayList<String> userAccess;
	private Timestamp created;
	private Timestamp lastUpdated;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public ArrayList<String> getUserAccess() {
		return userAccess;
	}
	public void setUserAccess(ArrayList<String> userAccess) {
		this.userAccess = userAccess;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	public Timestamp getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
}
