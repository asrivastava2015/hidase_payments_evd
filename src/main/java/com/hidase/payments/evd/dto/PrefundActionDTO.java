package com.hidase.payments.evd.dto;

public class PrefundActionDTO {

	private String transactionId;
	private String systemTraceId;
	private String status;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSystemTraceId() {
		return systemTraceId;
	}
	public void setSystemTraceId(String systemTraceId) {
		this.systemTraceId = systemTraceId;
	}
	@Override
	public String toString() {
		return "PrefundActionDTO [transactionId=" + transactionId + ", systemTraceId=" + systemTraceId + ", status="
				+ status + "]";
	}
	
	
}
