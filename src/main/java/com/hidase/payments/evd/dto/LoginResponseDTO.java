package com.hidase.payments.evd.dto;

import java.sql.Timestamp;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginResponseDTO {

    private String token;

    private Timestamp tokenExpiry;

    private boolean mfaRequired;
    
    private long userId;
    
    private long accountId;
    
    private boolean tempPassword;
}
