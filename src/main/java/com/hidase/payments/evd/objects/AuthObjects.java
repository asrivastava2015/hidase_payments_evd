package com.hidase.payments.evd.objects;

import com.hidase.payments.evd.auth.BasicAuth;

public class AuthObjects {
 
	private static BasicAuth basicAuth=null;
	
	public static BasicAuth basicAuth() {
		if(basicAuth==null) {
	    basicAuth=new BasicAuth();
		}
		return basicAuth;
	}
}
