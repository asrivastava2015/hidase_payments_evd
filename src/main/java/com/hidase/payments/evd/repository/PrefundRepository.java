package com.hidase.payments.evd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hidase.payments.evd.entities.Prefund;


public interface PrefundRepository  extends JpaRepository<Prefund, Long>{

	@Query("SELECT u FROM Prefund u WHERE u.userId = ?1")
	List<Prefund> findPrefundByUserId(long userId);
	
	@Query("SELECT u FROM Prefund u WHERE u.accountId = ?1")
	List<Prefund> findPrefundByAccountId(long accountId);
}
