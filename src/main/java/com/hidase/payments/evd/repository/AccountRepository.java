package com.hidase.payments.evd.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.hidase.payments.evd.entities.Account;





@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{

	@Query("SELECT u FROM Account u WHERE u.accountNumber = ?1")
	Account findByAccountNumber(String accountNumber);
	
	@Query("SELECT u FROM Account u WHERE u.userId = ?1")
	Account findByUserId(long userid);
}
