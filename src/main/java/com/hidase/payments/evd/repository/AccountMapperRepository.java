package com.hidase.payments.evd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.hidase.payments.evd.entities.AccountMapper;


public interface AccountMapperRepository extends JpaRepository<AccountMapper, Long>{

	@Query("SELECT u FROM AccountMapper u WHERE u.primaryAccountId = ?1")
	List<AccountMapper> findAccountsByprimaryAccountId(long primaryaccountid);
	@Query("SELECT u FROM AccountMapper u WHERE u.secondaryAccountId = ?1")
	AccountMapper findAccountsBysecondaryAccountId(long secondaryaccountid);
	
}
