package com.hidase.payments.evd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.hidase.payments.evd.entities.UserToken;


@Repository
public interface UserTokenRepository extends JpaRepository<UserToken, Long> {
	@Query("SELECT u FROM UserToken u WHERE u.userId = ?1")
    UserToken findByUserId(long userId);
	
}
