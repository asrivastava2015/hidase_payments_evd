package com.hidase.payments.evd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.hidase.payments.evd.entities.Role;





public interface RoleRepository extends JpaRepository<Role,Long> {

	@Query("SELECT u FROM Role u WHERE u.userId = ?1")
	Role findUserByUserId(long userId);
}
