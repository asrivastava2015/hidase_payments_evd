package com.hidase.payments.evd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hidase.payments.evd.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

	@Query("SELECT u FROM User u WHERE u.email = ?1")
	User findUserByEmail(String email);

	@Query("SELECT u FROM User u WHERE u.mobileNumber = ?1")
	User findUserByMobileNumber(String mobileNumber);
	
	@Query("SELECT u FROM User u WHERE u.token = ?1")
	User findUserByToken(String token);
}
