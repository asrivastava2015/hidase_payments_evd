package com.hidase.payments.evd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.hidase.payments.evd.entities.Transactions;

public interface TransactionsRepository extends JpaRepository<Transactions, Long>{

	@Query("SELECT u FROM Transactions u WHERE u.accountId = ?1")
	List<Transactions> findTransactionsByAccountId(long accountId);

	@Query(nativeQuery=true,value="SELECT * FROM transactions t WHERE t.account_id = ?1 and t.entity_type=?2")
	List<Transactions> findTypeTransactions(long accountId, String type);
	
	
}
