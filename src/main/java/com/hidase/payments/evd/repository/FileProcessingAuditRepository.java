package com.hidase.payments.evd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.hidase.payments.evd.entities.FileAudit;


public interface FileProcessingAuditRepository extends JpaRepository<FileAudit, Integer>{

	@Query("SELECT u FROM FileAudit u WHERE u.batchNumber = ?1")
	FileAudit findByBatchNumber(String batchNumber);

}
