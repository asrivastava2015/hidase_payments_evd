package com.hidase.payments.evd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hidase.payments.evd.entities.Pin;

public interface PinRepository extends JpaRepository<Pin, Long> {

	@Query(nativeQuery=true,value="SELECT * FROM pin p WHERE p.face_value = ?1 and p.status=?2 Limit 1")
	Pin findByFaceValue(double faceValue, String status);

}
