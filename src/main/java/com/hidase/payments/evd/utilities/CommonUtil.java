package com.hidase.payments.evd.utilities;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.hidase.payments.evd.client.TransactionResponse;
import com.hidase.payments.evd.constants.HidasiePaymentsConstants;
import com.hidase.payments.evd.dto.MobileTopupDTO;
import com.hidase.payments.evd.dto.PinDTO;
import com.hidase.payments.evd.dto.TopUpDTO;
import com.hidase.payments.evd.entities.Account;
import com.hidase.payments.evd.entities.AccountMapper;
import com.hidase.payments.evd.entities.EntityType;
import com.hidase.payments.evd.entities.Pin;
import com.hidase.payments.evd.entities.Prefund;
import com.hidase.payments.evd.entities.TransactionStatus;
import com.hidase.payments.evd.entities.TransactionType;
import com.hidase.payments.evd.entities.Transactions;
import com.hidase.payments.evd.entities.User;
import com.hidase.payments.evd.exception.InvalidAccessException;
import com.hidase.payments.evd.exception.PinRepositoryException;
import com.hidase.payments.evd.exception.PrimaryAccountNotFoundException;
import com.hidase.payments.evd.repository.AccountMapperRepository;
import com.hidase.payments.evd.repository.AccountRepository;
import com.hidase.payments.evd.repository.PinRepository;
import com.hidase.payments.evd.repository.PrefundRepository;
import com.hidase.payments.evd.repository.TransactionsRepository;


@Component
public class CommonUtil {
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	@Autowired
	private PrefundRepository prefundRepo;

	@Autowired
	private AccountRepository accountRepo;

	@Autowired
	private AccountMapperRepository accountMapperRepo;

	@Autowired
	private TransactionsRepository transRepo;

	@Autowired
	private PinRepository pinRepo;
	
	@Autowired
	private JavaMailSender mailSender;
	
	public static String generateAccountNumber() {
		String number = generateRandomNumber() + generateRandomNumber1();

		return number;
	}

	public static String generateTransactionId() {
		String id = generateTransid() + generateTransid1();

		return id;
	}

	public static String generateRandomNumber() {
		int n = 7;
		int m = (int) Math.pow(7, n - 1);
		int k = m + new Random().nextInt(9 * m);

		return Integer.toString(k);
	}

	public static String generateTransid() {
		int n = 6;
		int m = (int) Math.pow(6, n - 1);
		int k = m + new Random().nextInt(9 * m);

		return Integer.toString(k);
	}

	public static String generateTransid1() {
		int n = 7;
		int m = (int) Math.pow(7, n - 1);
		int k = m + new Random().nextInt(9 * m);

		return Integer.toString(k);
	}

	public static String generateRandomNumber1() {
		int n = 5;
		int m = (int) Math.pow(5, n - 1);
		int k = m + new Random().nextInt(9 * m);

		return Integer.toString(k);
	}

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public static Timestamp getTimeStamp() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		return timestamp;
	}

	public static long getEpoc() {
		Date d = new Date();
		long epoc = d.getTime();
		return epoc;
	}

	public static Timestamp getTokenExpiry() {
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("EAT"));
		System.out.println("2^^^^^your time here"+cal.getTime());
		cal.add(Calendar.MINUTE, 10);
		Timestamp timestamp = new Timestamp(cal.getTime().getTime());
		System.out.println("2^^^^^expire token in"+timestamp);
		return timestamp;
	}
	
	public static Timestamp getResetPasswordExpiry() {
		Timestamp timestamp = new Timestamp(new Date().getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp.getTime());
		cal.add(Calendar.MINUTE, 5);
		timestamp = new Timestamp(cal.getTime().getTime());
		return timestamp;
	}

	public static boolean isNullValue(String value) {
		return (value == null || value.trim().length() == 0 || value.trim().equalsIgnoreCase("null")) ? true : false;
	}

	public static String hashPassword(String plainTextPassword) {
		return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
		
	}

	public static boolean checkPass(String plainPassword, String hashedPassword) {
		if (BCrypt.checkpw(plainPassword, hashedPassword)) {
			return true;
		}
		return false;
	}

	public static HashMap<Integer, String> userAcessMap() {
		HashMap<Integer, String> userAccess = new HashMap<>();
		userAccess.put(1, "createAccountAndUser");
		userAccess.put(2, "retrieveAll");
		userAccess.put(3, "updateUser");
		userAccess.put(4, "deleteUser");
		userAccess.put(5, "deleteAccount");
		userAccess.put(6, "createUser");
		userAccess.put(7, "retrieve");
		userAccess.put(8, "createUserExistingAccount");
		userAccess.put(9, "topUp");
		userAccess.put(10, "prefundRequest");
		userAccess.put(11, "prefundAction");
		userAccess.put(12, "uploadFile");
		userAccess.put(13, "All");

		return userAccess;

	}

	public PinDTO fetchPinDetails(Account account, User user, double facevalue)
			throws ParseException, ClassNotFoundException, SQLException {
		PinDTO pinObj = new PinDTO();
		String status = "UNUSED";
		String updateStatus = "USED";
		String accountid = Long.toString(account.getId());
		
		Pin pin=pinRepo.findByFaceValue(facevalue,status);
		
		if(pin==null) {
			throw new PinRepositoryException("No pin is available for the amount provided");
		}
		
		pinObj.setId(pin.getId());
		pinObj.setFaceValue(pin.getFaceValue());
		pinObj.setPin(pin.getPin());
		pinObj.setSerial(pin.getSerial());
		pinObj.setExpiry(getExpiryDate(pin.getExpiry()));
		
		Optional<Pin> pinObject=pinRepo.findById(pin.getId());
		Pin pin1=pinObject.get();
		pin1.setAccountId(accountid);
		pin1.setStatus(updateStatus);
		pinRepo.save(pin1);
		return pinObj;
	}

	private static Date getExpiryDate(String expiry) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.parse(expiry);
	}

	public void verifyThePrefundId(User user, String authString, long prefundId) {
		if (prefundRepo.existsById(prefundId)) {
			Optional<Prefund> prefundObj = prefundRepo.findById(prefundId);
			Prefund prefund = prefundObj.get();
			String accountId = Long.toString(prefund.getAccountId());
			Account account = accountRepo.findByUserId(user.getId());
			AccountMapper accountMapper = accountMapperRepo
					.findAccountsBysecondaryAccountId(Integer.parseInt(accountId));

			if (accountMapper == null) {
				throw new PrimaryAccountNotFoundException("No primary account found for provided prefundId");
			}
			if (accountMapper.getPrimaryAccountId() == account.getId()) {
				return;
			}
			if (account.getId() != Integer.parseInt(accountId)) {
				throw new InvalidAccessException("You are not authorize to perform the action on provided prefundId");
			}

		}

	}

	public void verifyTheUserAccountId(User user, String authString, long accountId) {
		Account account = accountRepo.findByUserId(user.getId());
        AccountMapper mapper=accountMapperRepo.findAccountsBysecondaryAccountId(accountId);
        
		if (account.getId() != accountId) {
			throw new InvalidAccessException("You are not authorize to perform the action on provided account Id");
		}
	}

	public long verifyprefundAction(User user, String authString, long prefundId) {

		long primaryAccountId = 0;
		if (prefundRepo.existsById(prefundId)) {
			Optional<Prefund> prefundObj = prefundRepo.findById(prefundId);
			Prefund prefund = prefundObj.get();
			String accountId = Long.toString(prefund.getAccountId());
			Account account = accountRepo.findByUserId((int) user.getId());
			AccountMapper accountMapper = accountMapperRepo
					.findAccountsBysecondaryAccountId(Integer.parseInt(accountId));

			if (accountMapper == null) {
				throw new PrimaryAccountNotFoundException("No primary account found for provided prefundId");
			}
			primaryAccountId = accountMapper.getPrimaryAccountId();
			if (accountMapper.getPrimaryAccountId() != account.getId()) {
				throw new InvalidAccessException(
						"You are not authorize to modify the prefund for the provided prefundId");
			}

		}
		return primaryAccountId;
	}

	public void verifyTheUserId(User user, String authString, long userId) {
		long requestUserId = user.getId();
		if (requestUserId != userId) {
			throw new InvalidAccessException("You are not authorize to view the details of provided user id");
		}

	}

	public void addPrefundTransactions(Account primaryAccont, Account acct, Prefund prefundDetail) {
		// Adding transactions for primary account
		Transactions primaryAccountTrans = new Transactions();
		primaryAccountTrans.setType(TransactionType.DEBIT);
		primaryAccountTrans.setAmount(Double.parseDouble(prefundDetail.getAmount()));
		primaryAccountTrans
				.setDescription(getPrefundDescription(HidasiePaymentsConstants.DEBIT, primaryAccont, acct, prefundDetail));
		primaryAccountTrans.setCurrency(primaryAccont.getCurrency());
		primaryAccountTrans.setAccountId(primaryAccont.getId());
		primaryAccountTrans.setEntityType(EntityType.PREFUND_TRANSACTION);
		primaryAccountTrans.setStatus(TransactionStatus.APPROVED);
		primaryAccountTrans.setReferenceId(prefundDetail.getTransactionId());
		primaryAccountTrans.setPostingDate(getTimeStamp());
		primaryAccountTrans.setCreated(getTimeStamp());
		primaryAccountTrans.setLastUpdated(getTimeStamp());
		transRepo.save(primaryAccountTrans);

		// Adding transactions for secondary account
		Transactions secondaryAccountTrans = new Transactions();
		secondaryAccountTrans.setType(TransactionType.CREDIT);
		secondaryAccountTrans.setAmount(Double.parseDouble(prefundDetail.getAmount()));
		secondaryAccountTrans
				.setDescription(getPrefundDescription(HidasiePaymentsConstants.CREDIT, primaryAccont, acct, prefundDetail));
		secondaryAccountTrans.setCurrency(acct.getCurrency());
		secondaryAccountTrans.setAccountId(acct.getId());
		secondaryAccountTrans.setEntityType(EntityType.PREFUND_TRANSACTION);
		secondaryAccountTrans.setStatus(TransactionStatus.APPROVED);
		secondaryAccountTrans.setReferenceId(prefundDetail.getTransactionId());
		secondaryAccountTrans.setPostingDate(getTimeStamp());
		secondaryAccountTrans.setCreated(getTimeStamp());
		secondaryAccountTrans.setLastUpdated(getTimeStamp());
		transRepo.save(secondaryAccountTrans);
	}

	private String getPrefundDescription(String type, Account primaryAccont, Account acct, Prefund prefundDetail) {
		String desc = "";
		if (type.contains(HidasiePaymentsConstants.DEBIT)) {
			String primaryaccountNumber = primaryAccont.getAccountNumber();
			primaryaccountNumber = "xxxxxxx" + primaryaccountNumber.substring(primaryaccountNumber.length() - 4);
			String secondaryaccountNumber = acct.getAccountNumber();
			secondaryaccountNumber = "xxxxxxx" + secondaryaccountNumber.substring(secondaryaccountNumber.length() - 4);
			desc = "Account-" + primaryaccountNumber +" "+ "Transfered Prefund Amount To" + " " + "Account-"
					+ secondaryaccountNumber +" "+ "RefNum-" + prefundDetail.getTransactionId();
		} else {
			String primaryaccountNumber = primaryAccont.getAccountNumber();
			primaryaccountNumber = "xxxxxxx" + primaryaccountNumber.substring(primaryaccountNumber.length() - 4);
			String secondaryaccountNumber = acct.getAccountNumber();
			secondaryaccountNumber = "xxxxxxx" + secondaryaccountNumber.substring(secondaryaccountNumber.length() - 4);
			desc = "Account-" + secondaryaccountNumber + " " + "Received Prefund Amount from" + " " + "Account-"
					+ primaryaccountNumber + " " + "RefNum-" + prefundDetail.getTransactionId();
		}
		return desc;
	}

	public void addTopUpTransactions(Account account, TopUpDTO topUp) {
		
		Transactions topUpTransactions=new Transactions();
		topUpTransactions.setType(TransactionType.DEBIT);
		topUpTransactions.setAmount(topUp.getAmount());
		topUpTransactions
				.setDescription(getTopUpDescription(HidasiePaymentsConstants.DEBIT, account, topUp));
		topUpTransactions.setCurrency(account.getCurrency());
		topUpTransactions.setAccountId(account.getId());
		topUpTransactions.setEntityType(EntityType.TOP_UP_TRANSACTION);
		topUpTransactions.setStatus(TransactionStatus.SUCCESS);
		topUpTransactions.setReferenceId(generateTransactionId());
		topUpTransactions.setPostingDate(getTimeStamp());
		topUpTransactions.setCreated(getTimeStamp());
		topUpTransactions.setLastUpdated(getTimeStamp());
		transRepo.save(topUpTransactions);
		
	}

	private String getTopUpDescription(String type, Account account, TopUpDTO topUp) {
		String mobileNumber = "";
		String desc = "";
		if (topUp.getMobileNumber() != null) {
			mobileNumber = "xxxxxx" + topUp.getMobileNumber().substring(topUp.getMobileNumber().length() - 4);
		}
		if (!mobileNumber.isEmpty()) {
			desc = mobileNumber + " " + "Top Up with amount" + " " + topUp.getAmount();
		} else {
			desc = "Top Up with amount" + " " + topUp.getAmount();
		}
		return desc;
	}

	public static String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        System.out.println("2^^get site url"+siteURL);
        String url=siteURL.replace(request.getServletPath(), "");
        System.out.println("2^^get  url here"+siteURL);
        return url;
    }
	
	public void sendEmail(String recipientEmail, String tempPassword)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);
         
        helper.setFrom("etopup@hidasietele.com", "Hidase Support");
        helper.setTo(recipientEmail);
         
        String subject = "Here's the link to reset your password";
         
        String content = "<p>Dear Coustomer,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Your temporary password is : "+tempPassword+"</p>"
                + "<p>User this password to login to your account. This password is valid for 30 mintues</p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password "
                + "or report if you have not made the request.</p>"
                +"<p>This is an auto-generated email. Please do not reply</p>"
                +"<br>"
                + "<p>Warm Regards,</p>"
                +"<p>Hidase Support.</p>";
         
        helper.setSubject(subject);
         
        helper.setText(content, true);
         
        mailSender.send(message);
    }
	
	public void addPrepaidTransactions(Account account, MobileTopupDTO topUp, TransactionStatus status, TransactionResponse transactionResponse) {

		System.out.println("2^^^^^prepaid Transaction status"+status);
		System.out.println("2^^^^^prepaid Transaction success here for"+status.equals(TransactionStatus.SUCCESS));
		System.out.println("2^^^^^prepaid Transaction failed here for"+status.equals(TransactionStatus.FAILED));
		if(status.equals(TransactionStatus.SUCCESS)) {
		Transactions prePaidTransactions=new Transactions();
		prePaidTransactions.setType(TransactionType.DEBIT);
		prePaidTransactions.setAmount(topUp.getAmount());
		prePaidTransactions
				.setDescription(getprePaidDescription(HidasiePaymentsConstants.DEBIT, account, topUp,status));
		prePaidTransactions.setCurrency(account.getCurrency());
		prePaidTransactions.setAccountId(account.getId());
		prePaidTransactions.setEntityType(EntityType.PREPAID_TRANSACTION);
		prePaidTransactions.setExtReferenceId(transactionResponse.getExtReferenceId());
		prePaidTransactions.setStatus(TransactionStatus.SUCCESS);
		prePaidTransactions.setReferenceId(topUp.getReferenceId());
		prePaidTransactions.setPostingDate(getTimeStamp());
		prePaidTransactions.setCreated(getTimeStamp());
		prePaidTransactions.setLastUpdated(getTimeStamp());
		transRepo.save(prePaidTransactions);
		}else if(status.equals(TransactionStatus.FAILED)) {
			Transactions prePaidTransactions=new Transactions();
			prePaidTransactions.setDescription(getprePaidDescription(null, account, topUp,status));
			prePaidTransactions.setAmount(topUp.getAmount());
			prePaidTransactions.setCurrency(account.getCurrency());
			prePaidTransactions.setReferenceId(topUp.getReferenceId());
			prePaidTransactions.setEntityType(EntityType.PREPAID_TRANSACTION);
			prePaidTransactions.setAccountId(account.getId());
			prePaidTransactions.setStatus(TransactionStatus.FAILED);
			prePaidTransactions.setPostingDate(getTimeStamp());
			prePaidTransactions.setCreated(getTimeStamp());
			prePaidTransactions.setLastUpdated(getTimeStamp());
			transRepo.save(prePaidTransactions);
		}
		

	}
	
	private String getprePaidDescription(String type, Account account, MobileTopupDTO topUp,TransactionStatus status) {
		String mobileNumber="";
		String desc="";
		if(status.equals(TransactionStatus.SUCCESS)) {
		mobileNumber="xxxxxx"+topUp.getMobileNumber().substring(topUp.getMobileNumber().length() - 4);
		desc=mobileNumber+ " "+"Top Up with amount"+" "+topUp.getAmount();
		}else if(status.equals(TransactionStatus.FAILED)) {
			mobileNumber="xxxxxx"+topUp.getMobileNumber().substring(topUp.getMobileNumber().length() - 4);
			desc=mobileNumber+ " "+"Top Up with amount"+" "+topUp.getAmount()+" "+"failed";
		}
		return desc;
	}

	public static void setTransactionId(MobileTopupDTO req) {
		req.setReferenceId(generateTransactionId());
		
	}

}
