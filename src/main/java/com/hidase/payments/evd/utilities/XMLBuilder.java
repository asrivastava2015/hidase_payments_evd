package com.hidase.payments.evd.utilities;

import com.hidase.payments.evd.constants.RechargeConstant;
import com.hidase.payments.evd.dto.MobileTopupDTO;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.regex.Pattern;

public class XMLBuilder {
    public static String rechargeRequest(MobileTopupDTO req) {

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            doc.setXmlStandalone(true);

            Element rootElement = doc.createElement("COMMAND");
            doc.appendChild(rootElement);

            Element telement = doc.createElement("TYPE");
            System.out.println("2^^^^^service type is"+req.getServiceType());
            setServiceType(req);
            System.out.println("2^^^^^after setting service type is"+req.getServiceType());
            telement.appendChild(doc.createTextNode(req.getServiceType()));
            rootElement.appendChild(telement);

            Element delement = doc.createElement("DATE");
            delement.appendChild(doc.createTextNode(req.getCurrentDate1()));
            rootElement.appendChild(delement);

            Element extnwElement = doc.createElement("EXTNWCODE");
            extnwElement.appendChild(doc.createTextNode(RechargeConstant.EXTNWCODE));
            rootElement.appendChild(extnwElement);

            Element msisdnElement = doc.createElement("MSISDN");
            msisdnElement.appendChild(doc.createTextNode(RechargeConstant.MSISDN));
            rootElement.appendChild(msisdnElement);

            Element pinElement = doc.createElement("PIN");
            pinElement.appendChild(doc.createTextNode(RechargeConstant.PIN));
            rootElement.appendChild(pinElement);

            rootElement.appendChild(doc.createTextNode("<LOGINID></LOGINID>"));
            rootElement.appendChild(doc.createTextNode("<PASSWORD></PASSWORD>"));
            rootElement.appendChild(doc.createTextNode("<EXTCODE></EXTCODE>"));
            Element extRefNumElement = doc.createElement("EXTREFNUM");
            extRefNumElement.appendChild(doc.createTextNode(req.getReferenceId()));
            rootElement.appendChild(extRefNumElement);

            Element msisdn2Element = doc.createElement("MSISDN2");
            msisdn2Element.appendChild(doc.createTextNode(req.getMobileNumber() != null ? req.getMobileNumber() : ""));
            rootElement.appendChild(msisdn2Element);

            Element amountElement = doc.createElement("AMOUNT");
            amountElement.appendChild(doc.createTextNode(Double.toString(req.getAmount())));
            rootElement.appendChild(amountElement);

            Element lang1Element = doc.createElement("LANGUAGE1");
            lang1Element.appendChild(doc.createTextNode(RechargeConstant.LANGUAGE_EN));
            rootElement.appendChild(lang1Element);

            Element lang2Element = doc.createElement("LANGUAGE2");
            lang2Element.appendChild(doc.createTextNode(RechargeConstant.LANGUAGE_AMHARIC));
            rootElement.appendChild(lang2Element);

            Element selector = doc.createElement("SELECTOR");
            selector.appendChild(doc.createTextNode(Integer.toString(RechargeConstant.SELECTOR_RECHARGE)));
            rootElement.appendChild(selector);

            String[] str = getStringFromDocument(doc).split(Pattern.quote("encoding=\"UTF-8\""));

            System.out.println("telecom request=" + (str[0].trim() + str[1]).replaceAll("&lt;", "<").replaceAll("&gt;", ">"));
            return (str[0].trim() + str[1]).replaceAll("&lt;", "<").replaceAll("&gt;", ">");

        } catch (Exception e) {
            return null;
        }
    }



    private static void setServiceType(MobileTopupDTO req) {
		if(req.getServiceType().equals("PREPAID")) {
			req.setServiceType("EXRCTRFREQ");
		}
		
	}



	public static String getStringFromDocument(Document doc) {
        try {
            DOMSource domSource = new DOMSource(doc);
            StringWriter stringWriter = new StringWriter();
            StreamResult streamResult = new StreamResult(stringWriter);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, streamResult);
            //	transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
