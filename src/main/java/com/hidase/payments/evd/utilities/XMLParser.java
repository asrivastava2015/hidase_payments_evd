package com.hidase.payments.evd.utilities;

import com.hidase.payments.evd.client.RechargeResponse;
import com.hidase.payments.evd.client.ResponseStatus;
import com.hidase.payments.evd.client.TransactionResponse;
import com.hidase.payments.evd.constants.RechargeConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

public class XMLParser {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	public static TransactionResponse parseRechargeResponse(String response) {
		TransactionResponse resp = new TransactionResponse();
		RechargeResponse rechargeResp = new RechargeResponse();
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(response)));
			doc.getDocumentElement().normalize();
			System.out.println("rootElement:: " + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("COMMAND");
			Node nNode = nList.item(0);
			System.out.println("\nCurrent Element: " + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) nNode;
				String status = element.getElementsByTagName("TXNSTATUS").item(0).getTextContent();
				System.out.println("status==" + status);

				rechargeResp.setRechargeType(element.getElementsByTagName("TYPE").item(0).getTextContent() != null
						? element.getElementsByTagName("TYPE").item(0).getTextContent()
						: "NA");

				rechargeResp.setDate(element.getElementsByTagName("DATE").item(0).getTextContent() != null
						? element.getElementsByTagName("DATE").item(0).getTextContent()
						: "NA");

				rechargeResp.setOperatorId(element.getElementsByTagName("TXNID").item(0).getTextContent() != null
						? element.getElementsByTagName("TXNID").item(0).getTextContent()
						: "NA");

				rechargeResp.setTransactionId(element.getElementsByTagName("EXTREFNUM").item(0).getTextContent() != null
						? element.getElementsByTagName("EXTREFNUM").item(0).getTextContent()
						: "NA");
				System.out.println("EXTREFNUM==" + element.getElementsByTagName("EXTREFNUM").item(0).getTextContent());

				rechargeResp.setMessage(element.getElementsByTagName("MESSAGE").item(0).getTextContent() != null
						? element.getElementsByTagName("MESSAGE").item(0).getTextContent()
						: "NA");
				rechargeResp.setTrxnStatus(status);
				rechargeResp.setMessage(element.getElementsByTagName("MESSAGE").item(0).getTextContent());
				System.out.println("message==" + element.getElementsByTagName("MESSAGE").item(0).getTextContent());
				if (status != null && status.equalsIgnoreCase(RechargeConstant.RECHARGE_STATUS_SUCCESS)) {
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setMessage("Successfully get recharge response");
					resp.setSuccess(true);
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Invalid Mobile number");
				}
				resp.setExtrReferenceId(rechargeResp.getOperatorId());
				resp.setReferenceId(rechargeResp.getTransactionId());
			} else {
				resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
				resp.setMessage("Unable to process recharge");
			}
		} catch (Exception e) {
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Internal Server Error");
		}

		System.out.println("2^^^^^^^check recharge response message" + rechargeResp.getMessage());
		System.out.println("2^^^^^^^check recharge response operator id" + rechargeResp.getOperatorId());
		System.out.println("2^^^^^^^check recharge response recharge type" + rechargeResp.getRechargeType());
		System.out.println("2^^^^^^^check recharge response status" + rechargeResp.getReqStatus());
		System.out.println("2^^^^^^^check recharge response transction id" + rechargeResp.getTransactionId());
		System.out.println("2^^^^^^^check recharge response tran status" + rechargeResp.getTrxnStatus());
		return resp;
	}

}
