package com.hidase.payments.evd.pin.scheduler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.hidase.payments.evd.entities.FileAudit;
import com.hidase.payments.evd.entities.Pin;
import com.hidase.payments.evd.exception.FileReadIncorrectly;
import com.hidase.payments.evd.repository.FileProcessingAuditRepository;
import com.hidase.payments.evd.repository.PinRepository;
import com.hidase.payments.evd.services.PinServices;
import com.hidase.payments.evd.utilities.CommonUtil;



@Component
public class Scheduler {

	private String serial;
	private String pinValue;
	private double facevalue;
	private String filename;
	private String batchnumber;
	private String expiry;
	private double quantity;
	private double recordCount;
	private boolean startReading;
	private boolean fileAlreadyread;
	@Autowired
	private PinRepository pinRepo;
	@Autowired
	private PinServices pinService;
	@Autowired
	private FileProcessingAuditRepository fileAuditRepo;

	//@Scheduled(fixedDelay = 100000)
	public void cronJob() {
		fileAlreadyread = true;
		List<Pin> pinList = new ArrayList<>();
		filename = "C:\\Users\\asrivastava\\Downloads\\5BirrET.txt";
		FileAudit fileAudit = null;

		try {
			File myObj = new File(filename);
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				if (data.contains("PO Number:")) {
					continue;
				}
				if (data.contains("[END]")) {
					startReading = false;
					continue;
				}
				if (data.contains("Batch:")) {
					batchnumber = data.substring(data.indexOf(":") + 1);
				}
				if (fileAlreadyread) {
					fileAudit = pinService.findByBatchNumber(batchnumber);
					if (fileAudit == null) {
						fileAlreadyread = false;
					}
				}
				
				if (fileAudit != null) {
					boolean isRead = fileAudit.isRead();
					if (isRead) {
						break;
					}
				}
				Pin pin = new Pin();
				if (data.contains("Quantity:")) {
					quantity = Double.parseDouble(data.substring(data.indexOf(":") + 1));
				}
				if (data.contains("FaceValue:")) {
					facevalue = Double.parseDouble(data.substring(data.indexOf(":") + 1));
				}
				if (data.contains("StopDate:")) {
					expiry = data.substring(data.indexOf(":") + 1);
				}
				if (data.contains("[BEGIN]")) {
					startReading = true;
					continue;
				}
				if (startReading) {
					String[] pinData = data.split(" ");
					serial = pinData[0];
					pinValue = pinData[1];
					recordCount++;

					pin.setBatchNumber(batchnumber);
					pin.setExpiry(expiry);
					pin.setFaceValue(facevalue);
					pin.setPin(pinValue);
					pin.setSerial(serial);
					pin.setStatus("UNUSED");
					pin.setCustomId(pinValue+serial);
					pinList.add(pin);
				}
				
			}
			myReader.close();
			if (recordCount != quantity) {
				throw new FileReadIncorrectly("Number of enteries are not read correctly");
			}
			if (!pinList.isEmpty() && pinList.size() != 0) {
				for (int i = 0; i < pinList.size(); i++) {
					Pin pin = pinList.get(i);
					pinRepo.save(pin);
				}
			}
			if (fileAudit == null && pinList.size() != 0) {
				FileAudit file = new FileAudit();
				file.setFileName(filename);
				file.setBatchNumber(batchnumber);
				file.setRead(true);
				file.setReadOn(CommonUtil.getTimeStamp());
				fileAuditRepo.save(file);
			}
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
