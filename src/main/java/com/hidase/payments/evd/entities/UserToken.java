package com.hidase.payments.evd.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "user_token")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "token")
    private String token;

    @Column(name = "token_expiry")
    private Timestamp tokenExpiry;

    
    @Column(name = "mfa_required")
    private boolean mfaRequired;

    @Column(name = "mfa_token")
    private String mfaToken;

    @Column(name = "created")
    @CreationTimestamp
    private Timestamp created;

    @Column(name = "last_updated")
    @UpdateTimestamp
    private Timestamp lastUpdated;
    

}
