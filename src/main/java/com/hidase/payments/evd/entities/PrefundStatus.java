package com.hidase.payments.evd.entities;

public enum PrefundStatus {

    PENDING, APPROVED, DECLINED, CREATED;
}
