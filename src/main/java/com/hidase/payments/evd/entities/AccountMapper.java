package com.hidase.payments.evd.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "account_mapper")
@Getter
@Setter
public class AccountMapper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "primary_account_id")
    private long primaryAccountId;

    @Column(name = "secondary_account_id")
    private long secondaryAccountId;


}
