package com.hidase.payments.evd.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.sql.Timestamp;


@Getter
@Setter
@ToString
@Entity
@Table(name = "transactions")
public class Transactions {

    @Id
    @JsonProperty("id")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    @JsonProperty("type")
    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @Column(name = "amount")
    @JsonProperty("amount")
    private double amount;

    @Column(name = "description")
    @JsonProperty("description")
    private String description;

    @Column(name = "currency")
    @JsonProperty("currency")
    private String currency;

    @Column(name = "account_id")
    @JsonProperty("account_id")
    private long accountId;

    @Column(name = "entity_type")
    @JsonProperty("entity_type")
    @Enumerated(EnumType.STRING)
    private EntityType entityType;

    @Column(name = "status")
    @JsonProperty("status")
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    //Transaction_id
    @Column(name = "reference_id")
    @JsonProperty("reference_id")
    private String referenceId;

    @JsonProperty("postingDate")
    @Column(name = "posting_date")
    private Timestamp postingDate;

    @Column(name = "settlement_date")
    private Timestamp settlementDate;

    @CreationTimestamp
    @JsonProperty("created")
    @Column(name = "created")
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "last_updated")
    @JsonProperty("last_updated")
    private Timestamp lastUpdated;
    
    //External Transaction id
    @Column(name = "ext_reference_id")
    @JsonProperty("ext_reference_id")
    private String extReferenceId;

}
