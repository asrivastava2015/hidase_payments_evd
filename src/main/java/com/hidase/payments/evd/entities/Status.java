package com.hidase.payments.evd.entities;
public enum Status {

    ACTIVE, INACTIVE;
}
