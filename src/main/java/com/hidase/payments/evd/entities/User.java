package com.hidase.payments.evd.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "user")
@ToString
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, name = "email")
    private String email;

    @Column(unique = true, name = "mobile_number")
    private String mobileNumber;

    @Column(name = "password")
    private String password;

    @Column(name = "token")
    private String token;

    @Column(name = "token_expiry")
    private Timestamp tokenExpiry;

    @Column(name = "status")
    private String status;

    @Column(name = "created")
    @CreationTimestamp
    private Timestamp created;

    @Column(name = "last_updated")
    @UpdateTimestamp
    private Timestamp lastUpdated;
    
    @Column(name = "full_name")
    private String fullName;
    
    @Column(name = "temp_password")
    private String tempPassword;
    
    @Column(name = "is_temp_password")
    private boolean tempPwd;
    
    @Column(name = "temp_password_Expiry")
    private Timestamp tempPasswordExpiry;
    

}
