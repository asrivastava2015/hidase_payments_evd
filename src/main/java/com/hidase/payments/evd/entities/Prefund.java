package com.hidase.payments.evd.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;

import javax.persistence.*;


@Entity
@Table(name = "prefund")
@Getter
@Setter
@ToString
public class Prefund {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "amount")
    private String amount;

    @Column(name = "comment")
    private String comment;

    @Column(unique = true, name = "transaction_id")
    private String transactionId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private PrefundStatus status;

    @Column(name = "system_trace_id")
    private String systemTraceId;

    @Column(name = "date_of_transfer")
    private Timestamp dateOfTransfer;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "account_id")
    private long accountId;

    @Column(name = "created")
    @CreationTimestamp
    private Timestamp created;

    @Column(name = "last_updated")
    @UpdateTimestamp
    private Timestamp lastUpdated;

}
