package com.hidase.payments.evd.entities;

public enum TransactionStatus {

    BILLED, UNBILLED, ABORT, PENDING, INITIATED, APPROVED, SETTLED, DECLINED, RETRY, FLAGGED, FLAGGED_DUP,FAILED,SUCCESS;
}