package com.hidase.payments.evd.entities;

public enum TransactionType {

    DEBIT, CREDIT;
}
