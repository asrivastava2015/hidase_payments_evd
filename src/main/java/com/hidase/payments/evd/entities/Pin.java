package com.hidase.payments.evd.entities;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="pin")
public class Pin {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
    @Column(name="serial")
    private String serial;
	
	@NotNull
    @Column(name="pin")
    private String pin;
	
	@NotNull
    @Column(name="face_value")
    private double faceValue;
    
	@NotNull
    @Column(name="status")
    private String status;
	
	@NotNull
    @Column(name="batch_number")
    private String batchNumber;
	
	@NotNull
    @Column(name="expiry")
    private String expiry;
	
	@NotNull
    @Column(name="account_id")
    private String accountId;
	
	@NotNull
	@Column(name="custom_id",unique = true)
	private String customId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public double getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(double faceValue) {
		this.faceValue = faceValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCustomId() {
		return customId;
	}

	public void setCustomId(String customId) {
		this.customId = customId;
	}

	@Override
	public String toString() {
		return "Pin [id=" + id + ", serial=" + serial + ", pin=" + pin + ", faceValue=" + faceValue + ", status="
				+ status + ", batchNumber=" + batchNumber + ", expiry=" + expiry + ", accountId=" + accountId
				+ ", customId=" + customId + "]";
	}

	


}
