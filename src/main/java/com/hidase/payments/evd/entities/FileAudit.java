package com.hidase.payments.evd.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="file_audit")
public class FileAudit {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
    @Column(name="file_name")
    private String fileName;
	
	@NotNull
    @Column(name="batch_number",unique = true)
    private String batchNumber;
	
	@NotNull
    @Column(name="is_read")
    private boolean isRead;
	
	@NotNull
    @Column(name="read_on")
    private Timestamp readOn;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public Timestamp getReadOn() {
		return readOn;
	}

	public void setReadOn(Timestamp readOn) {
		this.readOn = readOn;
	}
}