package com.hidase.payments.evd.entities;

public enum EntityType {

    PREFUND_TRANSACTION("PREFUND_TRANSACTION"),
    PREPAID_TRANSACTION("PREPAID_TRANSACTION"),
    TOP_UP_TRANSACTION("TOP_UP_TRANSACTION");

    private final String literal;

    private EntityType(String literal) {
        this.literal = literal;
    }
}
