package com.hidase.payments.evd.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;

import javax.persistence.*;



@Entity
@Table(name = "account")
@Getter
@Setter
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "account_number", unique = true)
    private String accountNumber;

    @Column(name = "balance")
    private double balance;

    @Column(name = "created")
	@CreationTimestamp
    private Timestamp created;

    @Column(name = "last_updated")
	@UpdateTimestamp
    private Timestamp lastUpdated;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "currency")
    private String currency;

    @Column(name = "user_id")
    private long userId;

}
