package com.hidase.payments.evd;

import java.util.Date;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class HidasePaymentsEvdApplication {
	
	@PostConstruct
	void started() {
	    TimeZone.setDefault(TimeZone.getTimeZone("EAT"));
	}
	public static void main(String[] args) {
		SpringApplication.run(HidasePaymentsEvdApplication.class, args);
	}

	
}
